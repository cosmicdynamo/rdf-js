/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var webpackConfig = require('./webpack.config.js');
webpackConfig.entry = {};
var path = require('path');

function reportDir(type) {
  return function (browserName) {
    return path.join(type, browserName);
  }
}
module.exports = function (config) {
  config.set({
    basePath: '.',
    frameworks: [ 'mocha', 'chai' ],

    files: [
      'test.webpack.js'
    ],

    preprocessors: {
      'test.webpack.js': [ 'webpack', 'sourcemap' ],
      'test/**/*.js': [ 'webpack', 'sourcemap' ],
      'src/**/*.js': [ 'webpack', 'sourcemap' ]
    },

    webpack: { //kind of a copy of your webpack config + Istanbul
      devtool: 'inline-source-map',
      module: {
        preLoaders: [
          { test: /\.js$/, loader: 'isparta', include: path.join(__dirname, 'src/') }
        ],
        loaders: [
          {
            test: /.js$/,
            loader: 'babel',
            query: {
              presets: [ 'es2015', 'stage-0' ],
              sourceMap: 'inline'
            }
          }
        ]/*,
         postLoaders: [{
         test: /\.js/,
         include: /(src)/,
         loader: 'istanbul-instrumenter'
         }]*/
      },
      resolve: {
        alias: {
          'rdf-js': path.resolve("./")
        }
      }
    },
    webpackServer: {
      noInfo: true //please don't spam the console when running in karma!
    },

    reporters: [ 'progress', 'coverage' ],

    colors: true,
    autoWatch: false,

    browsers: [ /*'PhantomJS',*/ 'Chrome'/*, 'IE'*/ ],

    // optionally, configure the reporter
    coverageReporter: {
      dir: 'reports/coverage',
      reporters: [
        { type: 'html', subdir: reportDir('html') },
        { type: 'lcov', subdir: reportDir('lcov') },
        //{ type: 'cobertura', subdir: 'cobertura' },
        //{ type: 'lcovonly', subdir: 'lcovonly' },
        //{ type: 'teamcity', subdir: 'teamcity' },
        //{ type: 'text', subdir: 'text' },
        { type: 'text-summary', subdir: reportDir('test-summary') }
      ]
    }
  })
};
