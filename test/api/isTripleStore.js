/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import {typeTest} from "../../src";

/**
 * Method for testing if something is a DataSet
 * @method RdfJs.test.api.TripleStore
 * @param {*} test - The thing being tested
 * @param {string} [message] - A message to append if something is not correct
 */
function isTripleStore(test, message) {
  var reason = "";
  if (!test || !typeTest.isObject(test)) {
    reason = "object MUST be an Object";
  }
  if (typeTest.isFunction(test.addGraph)) {
    reason = "TripleStore requires an addGraph Method";
  } else if (typeTest.isFunction(test.runOnGraphs)) {
    reason = "TripleStore requires a runOnGraphs Method";
  } else if (typeTest.isFunction(test.setDefault)) {
    reason = "TripleStore requires a setDefault Method";
  } else if (typeTest.isFunction(test.add)) {
    reason = "TripleStore requires an add Method";
  } else if (typeTest.isFunction(test.addAll)) {
    reason = "TripleStore requires a addAll Method";
  } else if (typeTest.isFunction(test.remove)) {
    reason = "TripleStore requires a remove Method";
  } else if (typeTest.isFunction(test.removeMatches)) {
    reason = "TripleStore requires a removeMatches Method";
  } else if (typeTest.isFunction(test.toArray)) {
    reason = "TripleStore requires a toArray Method";
  } else if (typeTest.isFunction(test.some)) {
    reason = "TripleStore requires a some Method";
  } else if (typeTest.isFunction(test.every)) {
    reason = "TripleStore requires an every Method";
  } else if (typeTest.isFunction(test.filter)) {
    reason = "TripleStore requires a filter Method";
  } else if (typeTest.isFunction(test.forEach)) {
    reason = "TripleStore requires a forEach Method";
  } else if (typeTest.isFunction(test.match)) {
    reason = "TripleStore requires a match Method";
  } else if (typeTest.isFunction(test.getGraph)) {
    reason = "TripleStore requires a getGraph Method";
  }

  if (!!reason && !!message) {
    assert.fail("TripleStore", null, message + " " + reason);
  }
  return !!reason;
}

export default isTripleStore;
