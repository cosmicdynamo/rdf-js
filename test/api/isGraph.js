/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import {typeTest} from "../../src";
/**
 * Method for testing if something is an RDF Graph
 * @method RdfJs.test.api.Graph
 * @param {*} test - The thing being tested
 * @param {String} [message] - a message to output if test fails
 */
function isGraph(test, message) {
  var reason = "";
  if (!test || !typeTest.isObject(test)) {
    reason = "object MUST be an Object";
  }

  if (typeTest.isFunction(test.add)) {
    reason = "Graph requires a add Method";
  } else if (typeTest.isFunction(test.addAll)) {
    reason = "Graph requires a addAll Method";
  } else if (typeTest.isFunction(test.remove)) {
    reason = "Graph requires a remove Method";
  } else if (typeTest.isFunction(test.removeMatches)) {
    reason = "Graph requires a removeMatches Method";
  } else if (typeTest.isFunction(test.toArray)) {
    reason = "Graph requires a toArray Method";
  } else if (typeTest.isFunction(test.some)) {
    reason = "Graph requires a some Method";
  } else if (typeTest.isFunction(test.every)) {
    reason = "Graph requires a every Method";
  } else if (typeTest.isFunction(test.filter)) {
    reason = "Graph requires a filter Method";
  } else if (typeTest.isFunction(test.forEach)) {
    reason = "Graph requires a forEach Method";
  } else if (typeTest.isFunction(test.match)) {
    reason = "Graph requires a match Method";
  } else if (typeTest.isFunction(test.merge)) {
    reason = "Graph requires a merge Method";
  } else if (typeTest.isFunction(test.clone)) {
    reason = "Graph requires a clone Method";
  } else if (typeTest.isFunction(test.has)) {
    reason = "Graph requires a has Method";
  }


  if (!!reason && !!message) {
    assert.fail("Graph", null, message + " " + reason);
  }
  return !!reason;
}
export default isGraph;
