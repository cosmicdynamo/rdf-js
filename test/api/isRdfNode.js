/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 * @method RdfJs.test.api.Node
 * @param {*} instance
 */

import {typeTest} from "../../src";

function isNode(test, message) {
  var reason;
  if (!test) {
    reason = "not an Object";
  } else if (!typeTest.isObject(test)) {
    reason = "not an Object";
  } else if (!typeTest.isString(test.interfaceName)) {
    reason = "interfaceName MUST be a string";
  } else if (!typeTest.isString(test.nominalValue)) {
    reason = "nominalValue MUST be a string";
  } else if (!typeTest.isFunction(test.toString)) {
    reason = "RDF Node MUST contain a toString method";
  } else if (!typeTest.isFunction(test.valueOf)) {
    reason = "RDF Node MUST contain a valueOf method";
  } else if (!typeTest.isFunction(test.toNT)) {
    reason = "RDF Node MUST contain a toNT method";
  } else if (!typeTest.isFunction(test.equals)) {
    reason = "RDF Node MUST contain a equals method";
  } else if (!typeTest.isFunction(test.isBlank)) {
    reason = "RDF Node MUST contain a isBlank method";
  } else if (!typeTest.isFunction(test.isNamed)) {
    reason = "RDF Node MUST contain a isNamed method";
  } else if (!typeTest.isFunction(test.isLiteral)) {
    reason = "RDF Node MUST contain a isLiteral method";
  }

  if (!!message && !!reason) {
    assert.fail("RDF Node", null, message + ": " + reason);
  }
  return !!reason;
}

export default isNode;
