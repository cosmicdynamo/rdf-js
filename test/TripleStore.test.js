/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { TripleStore } from "../src";
import Triple from "./fake/Triple";
import Graph from "./fake/Graph";

describe("package - RdfJs", function () {
  describe("class - TripleStore", function () {
    beforeEach(function () {
      var store = new TripleStore({
        GraphCtor: Graph,
        default: "urn:ThirdGraph"
      });

      this.first = store.addGraph("urn:FirstGraph");
      this.second = store.addGraph("urn:SecondGraph");
      this.third = store.addGraph("urn:ThirdGraph");
      this.store = store;
    });

    describe("method - runOnGraphs", function () {
      it("Runs input function with the requested Graph", function () {
        this.store.runOnGraphs(function (graph) {
          graph.run = true;
        }, "urn:FirstGraph");

        assert.isTrue(this.first.run, "Graph 1 was hit");
        assert.isUndefined(this.second.run, "Graph 2 was NOT hit");
        assert.isUndefined(this.third.run, "DEFAULT was NOT hit");
      });
    });

    describe("method - runOnGraph", function () {
      it("Runs input function with the requested Graphs", function () {
        this.store.runOnGraphs(function (graph) {
          graph.run = true;
        }, [ "urn:FirstGraph", "urn:SecondGraph" ]);

        assert.isTrue(this.first.run, "Graph 1 was hit");
        assert.isTrue(this.second.run, "Graph 2 was hit");
        assert.isUndefined(this.third.run, "DEFAULT was NOT hit");
      });

      it("Runs input function for the DEFAULT Graph", function () {
        this.store.runOnGraphs(function (graph) {
          graph.run = true;
        }, "DEFAULT");

        assert.isUndefined(this.first.run, "Graph 1 was NOT hit");
        assert.isUndefined(this.second.run, "Graph 2 was NOT hit");
        assert.isTrue(this.third.run, "DEFAULT was hit");
      });

      it("With no target specified Runs input function for the DEFAULT Graph", function () {
        this.store.runOnGraphs(function (graph) {
          graph.run = true;
        });

        assert.isUndefined(this.first.run, "Graph 1 was NOT hit");
        assert.isUndefined(this.second.run, "Graph 2 was NOT hit");
        assert.isTrue(this.third.run, "DEFAULT was hit");
      });

      it("Runs input function for the DEFAULT Graphs", function () {
        this.store.setDefault([ "urn:SecondGraph", "urn:FirstGraph" ]);

        this.store.runOnGraphs(function (graph) {
          graph.run = true;
        });

        assert.isTrue(this.first.run, "Graph 1 was hit");
        assert.isTrue(this.second.run, "Graph 2 was hit");
        assert.isUndefined(this.third.run, "DEFAULT was NOT hit");
      });

      it("Runs input function for the DEFAULT=ALL Graphs", function () {
        this.store.setDefault("ALL");

        this.store.runOnGraphs(function (graph) {
          graph.run = true;
        });

        assert.isTrue(this.first.run, "Graph 1 was hit");
        assert.isTrue(this.second.run, "Graph 2 was hit");
        assert.isTrue(this.third.run, "DEFAULT was hit");
      });

      it("Runs input function for ALL Graphs", function () {
        this.store.runOnGraphs(function (graph) {
          graph.run = true;
        }, "ALL");

        assert.isTrue(this.first.run, "Graph 1 was hit");
        assert.isTrue(this.second.run, "Graph 2 was hit");
        assert.isTrue(this.third.run, "DEFAULT was hit");
      });
    });

    describe("method - add", function () {
      it("Adds Triple to the target Graph", function () {
        var fn = this.store.runOnGraphs;

        var runOnGraphsCalled = false;
        this.store.runOnGraphs = function (method, graph) {
          if (!runOnGraphsCalled) {
            runOnGraphsCalled = graph || "DEFAULT";
          }
          return fn.apply(this, arguments);
        };

        var expected = new Triple("<urn:Subject>", "<urn:PhasPredicate>", '"Object"');
        var actual = null;
        this.first.add = function (t) {
          actual = t;
        };
        this.third.add = this.second.add = () => {
          assert.fail(null, null, { message: "Triple should not be added here" });
        };

        this.store.add(expected, "urn:FirstGraph");

        assert.strictEqual(actual, expected, "Triple Added to First Graph");
      });
    });

    describe("method - remove", function () {
      it("Removes Triple from a Graph", function () {
        var fn = this.store.runOnGraphs;

        var runOnGraphsCalled = false;
        this.store.runOnGraphs = function (method, graph) {
          if (!runOnGraphsCalled) {
            runOnGraphsCalled = graph || "DEFAULT";
          }
          return fn.apply(this, arguments);
        };

        var expected = new Triple("<urn:Subject>", "<urn:PhasPredicate>", '"Object"');
        var actual = null;
        this.third.remove = function (t) {
          actual = t;
        };
        this.first.remove = this.second.add = () => {
          assert.fail(null, null, { message: "Triple should not be removed here" });
        };

        this.store.remove(expected);

        assert.strictEqual(runOnGraphsCalled, "DEFAULT", "runOnGraphs method was used to execute this request");
        assert.strictEqual(actual, expected, "Triple Added to First Graph");
      });
    });

    describe("method - removeMatches", function () {
      it("removes all triples from target Graph", function () {
        var fn = this.store.runOnGraphs;

        var runOnGraphsCalled = false;
        this.store.runOnGraphs = function (method, graph) {
          if (!runOnGraphsCalled) {
            runOnGraphsCalled = graph || "DEFAULT";
          }
          return fn.apply(this, arguments);
        };

        var match1 = null, match2 = null, match3 = null;
        this.third.removeMatches = function (v1, v2, v3) {
          match1 = v1;
          match2 = v2;
          match3 = v3;
        };
        this.first.removeMatches = this.second.removeMatches = () => {
          assert.fail(null, null, { message: "Triple should not be removed here" });
        };

        var p1 = { name: "param1" };
        var p2 = { name: "param2" };
        var p3 = { name: "param3" };
        this.store.removeMatches(p1, p2, p3);

        assert.strictEqual(runOnGraphsCalled, "DEFAULT", "runOnGraphs method was used to execute this request");
        assert.strictEqual(match1, p1, "Subject was passed as Subject to Graph#removeMatches");
        assert.strictEqual(match2, p2, "Predicate was passed as Predicate to Graph#removeMatches");
        assert.strictEqual(match3, p3, "Object was passed as Object to Graph#removeMatches");
      });
    });

    describe("method - toArray", function () {
      it("Returns an array containing Triples from target Graph(s)", function () {
        var fn = this.store.runOnGraphs;

        var runOnGraphsCalled = false;
        this.store.runOnGraphs = function (method, graph) {
          if (!runOnGraphsCalled) {
            runOnGraphsCalled = graph || "DEFAULT";
          }
          return fn.apply(this, arguments);
        };

        var string = "Graph1 Graph2 AndMore Really? Graph3";
        var values = string.split(" ");
        this.first.toArray = function () {
          return values.splice(0, 1);
        };
        this.second.toArray = function () {
          return values.splice(0, 2);
        };
        this.third.toArray = function () {
          return values;
        };

        var array = this.store.toArray("ALL");

        assert.strictEqual(runOnGraphsCalled, "ALL", "runOnGraphs method was used to execute this request");
        assert.strictEqual(array.length, 5);
        array.forEach(function (value) {
          string = string.replace(value, "found");
        });

        assert.strictEqual(string, "found found found found found");
      });
    });

    describe("method - some", function () {
      it("Runs fn and returns against target Graph", function () {
        var fn = this.store.runOnGraphs;

        var runOnGraphsCalled = false;
        this.store.runOnGraphs = function (method, graph) {
          if (!runOnGraphsCalled) {
            runOnGraphsCalled = graph || "DEFAULT";
          }
          return fn.apply(this, arguments);
        };
        var result = true;

        this.first.some = this.second.some = function () {
          return false;
        };
        this.third.some = function () {
          return result;
        };

        var tFilter = function () {
          return true;
        };

        assert.isTrue(this.store.some(tFilter, "ALL"), "Returns true when a Graph returns true");
        assert.strictEqual(runOnGraphsCalled, "ALL", "runOnGraphs method was used to execute this request");
        result = false;
        assert.isFalse(this.store.some(tFilter, "ALL"), "Returns false when no Graphs returns true");
      });
    });

    describe("method - evey", function () {
      it("Runs fn and returns against target Graph", function () {
        var fn = this.store.runOnGraphs;

        var runOnGraphsCalled = false;
        this.store.runOnGraphs = function (method, graph) {
          if (!runOnGraphsCalled) {
            runOnGraphsCalled = graph || "DEFAULT";
          }
          return fn.apply(this, arguments);
        };
        var result = true;

        this.first.every = this.second.every = function () {
          return true;
        };
        this.third.every = function () {
          return result;
        };

        var tFilter = function () {
          return true;
        };
        assert.isTrue(this.store.every(tFilter, "ALL"), "Returns true when a Graph returns true");
        assert.strictEqual(runOnGraphsCalled, "ALL", "runOnGraphs method was used to execute this request");

        result = false;
        assert.isFalse(this.store.every(tFilter, "ALL"), "Returns false when no Graphs returns true");
      });
    });

    describe("method - filter", function () {
      it("Returns filter result merged from target Graph(s)", function () {
        var fn = this.store.runOnGraphs;

        var runOnGraphsCalled = false;
        this.store.runOnGraphs = function (method, graph) {
          if (!runOnGraphsCalled) {
            runOnGraphsCalled = graph || "DEFAULT";
          }
          return fn.apply(this, arguments);
        };

        this.first.filter = function () {
          var g = new Graph();
          g.add(new Triple("<urn:FirstGraph>", "<urn:hasValue>", '"G1"'));
          return g;
        };
        this.second.filter = function () {
          var g = new Graph();
          g.add(new Triple("<urn:SecondGraph>", "<urn:hasValue>", '"G2"'));
          return g;
        };
        this.third.filter = function () {
          var g = new Graph();
          g.add(new Triple("<urn:ThirdGraph>", "<urn:hasValue>", '"G3"'));
          return g;
        };

        var tFilter = function () {
          return true;
        };
        var result = this.store.filter(tFilter, "ALL");

        Graph.testApi(result);
        assert.strictEqual(runOnGraphsCalled, "ALL", "runOnGraphs method was used to execute this request");
        assert.strictEqual(result.length, 3);

        assert.strictEqual(result.match("<urn:FirstGraph>", "<urn:hasValue>", '"G1"').length, 1, "First Value Found");
        assert.strictEqual(result.match("<urn:SecondGraph>", "<urn:hasValue>", '"G2"').length, 1, "Second Value Found");
        assert.strictEqual(result.match("<urn:ThirdGraph>", "<urn:hasValue>", '"G3"').length, 1, "Third Value Found");
      });
    });

    describe("method - forEach", function () {
      it("Runs fn on target Graph", function () {
        var fn = this.store.runOnGraphs;

        var runOnGraphsCalled = false;
        this.store.runOnGraphs = function (method, graph) {
          if (!runOnGraphsCalled) {
            runOnGraphsCalled = graph || "DEFAULT";
          }
          return fn.apply(this, arguments);
        };

        this.first.forEach = this.second.removeMatches = function () {
          assert.fail(null, null, { message: "Triple should not be removed here" });
        };
        this.third.forEach = function (callback) {
          callback("Triple");
        };

        var run = null;
        var tCallback = function (arg) {
          assert.strictEqual("Triple", arg);
          run = true;
        };

        this.store.forEach(tCallback);

        assert.strictEqual(runOnGraphsCalled, "DEFAULT", "runOnGraphs method was used to execute this request");
        assert.isTrue(run, "Callback called");
      });
    });

    describe("method - match", function () {
      it("Returns matched results merged from target Graph", function () {
        var fn = this.store.runOnGraphs;

        var runOnGraphsCalled = false;
        this.store.runOnGraphs = function (method, graph) {
          if (!runOnGraphsCalled) {
            runOnGraphsCalled = graph || "DEFAULT";
          }
          return fn.apply(this, arguments);
        };

        var match1 = null, match2 = null, match3 = null;
        this.first.match = function (v1, v2, v3) {
          match1 = v1;
          match2 = v2;
          match3 = v3;
          var g = new Graph();
          g.add(new Triple("<urn:FirstGraph>", "<urn:hasValue>", '"G1"'));
          return g;
        };
        this.second.match = function () {
          var g = new Graph();
          g.add(new Triple("<urn:SecondGraph>", "<urn:hasValue>", '"G2"'));
          return g;
        };
        this.third.match = function () {
          var g = new Graph();
          g.add(new Triple("<urn:ThirdGraph>", "<urn:hasValue>", '"G3"'));
          return g;
        };

        var p1 = { name: "param1" };
        var p2 = { name: "param2" };
        var p3 = { name: "param3" };
        var result = this.store.match(p1, p2, p3, "ALL");

        assert.strictEqual(runOnGraphsCalled, "ALL", "runOnGraphs method was used to execute this request");
        assert.strictEqual(match1, p1, "Subject was passed as Subject to Graph#removeMatches");
        assert.strictEqual(match2, p2, "Predicate was passed as Predicate to Graph#removeMatches");
        assert.strictEqual(match3, p3, "Object was passed as Object to Graph#removeMatches");

        assert.strictEqual(result.match("<urn:FirstGraph>", "<urn:hasValue>", '"G1"').length, 1, "First Value Found");
        assert.strictEqual(result.match("<urn:SecondGraph>", "<urn:hasValue>", '"G2"').length, 1, "Second Value Found");
        assert.strictEqual(result.match("<urn:ThirdGraph>", "<urn:hasValue>", '"G3"').length, 1, "Third Value Found");
      });
    });

    describe("method - addAll", function () {
      it("Adds Triples to target Graph", function () {
        var fn = this.store.runOnGraphs;

        var runOnGraphsCalled = false;
        this.store.runOnGraphs = function (method, graph) {
          if (!runOnGraphsCalled) {
            runOnGraphsCalled = graph || "DEFAULT";
          }
          return fn.apply(this, arguments);
        };

        var added = null;
        this.third.addAll = function (list) {
          added = list;
        };

        var p1 = new Graph();
        var s = "<urn:ThirdGraph>";
        var p = "<urn:hasValue>";
        var o = '"G3"';
        p1.add(new Triple(s, p, o));
        this.store.addAll(p1);

        assert.strictEqual(runOnGraphsCalled, "DEFAULT", "runOnGraphs method was used to execute this request");
        assert.strictEqual(added.length, p1.length, "input Graph was passed to target Graph#addAll");
        assert.strictEqual(added.match(s, p, o).length, 1);
      });
    });
  });
});
