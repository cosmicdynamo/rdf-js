/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { Triple } from "../src";

describe("package - RdfJs", function () {
  describe("class - Triple", function () {
    describe("method - equals", function () {
      it("returns true if input = self", function () {
        var list = [], list2 = [];
        var p = [ "<urn:hasValue>", "<urn:count>", "<urn:hasPuppies>" ];
        for (var sIdx = 1; sIdx < 10; sIdx++) {
          for (var pIdx = 0; pIdx < p.length; pIdx++) {
            for (var oIdx = 1; oIdx < 5; oIdx++) {
              list.push(new Triple("_:" + sIdx, p[ pIdx ], '"' + oIdx + '"^^<http://www.w3.org/2001/XMLSchema#int>'));
              list2.push(new Triple("_:" + sIdx, p[ pIdx ], '"' + oIdx + '"^^<http://www.w3.org/2001/XMLSchema#int>'));
            }
          }
        }

        for (var idx = 0; idx < list.length; idx++) {
          assert.isTrue(list[ idx ].equals(list2[ idx ]));
        }
      });

      it("Returns false if input != self", function () {
        var list = [], list2 = [];
        var p = [ "<urn:hasValue>", "<urn:count>", "<urn:hasPuppies>" ];
        for (var sIdx = 1; sIdx < 10; sIdx++) {
          for (var pIdx = 0; pIdx < p.length; pIdx++) {
            for (var oIdx = 1; oIdx < 5; oIdx++) {
              list.push(new Triple("_:" + sIdx, p[ pIdx ], '"' + oIdx + '"^^<http://www.w3.org/2001/XMLSchema#int>'));
              list2.push(new Triple("_:" + sIdx + 1, p[ pIdx ], '"' + oIdx + '"^^<http://www.w3.org/2001/XMLSchema#int>'));
            }
          }
        }

        for (var idx = 0; idx < list.length; idx++) {
          assert.isFalse(list[ idx ].equals(list2[ idx ]));
        }
      });
    });

    describe("method - toString", function () {
      it("Outputs a string in N-Triples notation", function () {
        var list = [];
        var p = [ "<urn:hasValue>", "<urn:count>", "<urn:hasPuppies>" ];
        for (var sIdx = 1; sIdx < 10; sIdx++) {
          for (var pIdx = 0; pIdx < p.length; pIdx++) {
            for (var oIdx = 1; oIdx < 5; oIdx++) {
              list.push(new Triple("_:" + sIdx, p[ pIdx ], '"' + oIdx + '"^^<http://www.w3.org/2001/XMLSchema#int>'));
            }
          }
        }

        for (var idx = 0; idx < list.length; idx++) {
          var t = list[ idx ];
          var nt = t.subject.toNT() + " " + t.predicate.toNT() + " " + t.object.toNT() + " .";
          assert.strictEqual(t.toString(), nt);
        }
      });
    });
  });
});
