/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { BlankNode, GraphBuilder, Literal, NamedNode } from "../src";
import { rdf } from "../src/prefix";

describe("package - rdf-js", function () {
  describe("module - GraphBuilder", function () {
    beforeEach(function () {
      this.builder = new GraphBuilder();
    });

    describe("method - createBlank", function () {
      it("returns a new blank node", function () {
        var bNode = this.builder.createBlank();

        assert.isTrue(bNode instanceof BlankNode);
      });

      it("passed value in to new blank node", function () {
        var bNode = this.builder.createBlank("000");

        assert.strictEqual(bNode.toNT(), "_:000");
      });
    });

    describe("method - createNamed", function () {
      it("returns a new named node", function () {
        var bNode = this.builder.createNamed();

        assert.isTrue(bNode instanceof NamedNode);
      });

      it("passed value in to new named node", function () {
        var bNode = this.builder.createNamed("http://example.com/test");

        assert.strictEqual(bNode.toNT(), "<http://example.com/test>");
      });
    });

    describe("method - createLiteral", function () {
      it("returns a new literal node", function () {
        var bNode = this.builder.createLiteral();

        assert.isTrue(bNode instanceof Literal);
      });

      it("passed value in to new literal node", function () {
        var bNode = this.builder.createLiteral("value");

        assert.strictEqual(bNode.toNT(), '"value"');
      });
    });

    describe("method - addTriple", function () {
      it("will add the input triple to the attached graph (strings)", function () {
        var subject = "<http://example.com/subject>";
        var predicate = "<http://example.com/test.owl#hasValue>";
        var object = "<http://example.com/object>";
        this.builder.addTriple(subject, predicate, "<http://example.com/object>");

        assert.strictEqual(this.builder.graph.length, 1, "One Triple in graph");
        assert.strictEqual(this.builder.graph.match(subject, predicate, object).length, 1, "It was the expected Triple");
      });

      it("will add the input triple to the attached graph (nodes)", function () {
        var subject = new NamedNode("http://example.com/subject");
        var predicate = new NamedNode("http://example.com/test.owl#hasValue");
        var object = new NamedNode("http://example.com/object");
        this.builder.addTriple(subject, predicate, object);

        assert.strictEqual(this.builder.graph.length, 1, "One Triple in graph");
        assert.strictEqual(this.builder.graph.match(subject.toNT(), predicate.toNT(), object.toNT()).length, 1, "It was the expected Triple");
      });
    });

    describe("method - add", function () {
      it("will add a triple for the predicate and object to the attached graph (strings)", function () {
        this.builder.add("<http://example.com/test.owl#hasValue>", "<http://example.com/object>");

        assert.strictEqual(this.builder.graph.length, 1, "One Triple in graph");
        assert.strictEqual(this.builder.graph.match(this.builder.subject.toNT(), "<http://example.com/test.owl#hasValue>", "<http://example.com/object>").length, 1, "It was the expected Triple");
      });

      it("will add a triple for the predicate and object to the attached graph (nodes)", function () {
        var predicate = new NamedNode("http://example.com/test.owl#hasValue");
        var object = new NamedNode("http://example.com/object");
        this.builder.add(predicate, object);

        assert.strictEqual(this.builder.graph.length, 1, "One Triple in graph");
        assert.strictEqual(this.builder.graph.match(this.builder.subject.toNT(), "<http://example.com/test.owl#hasValue>", "<http://example.com/object>").length, 1, "It was the expected Triple");
      });
    });

    describe("method - removeTriple", function () {
      beforeEach(function () {
        this.subject = new NamedNode("http://example.com/subject");
        this.predicate = new NamedNode("http://example.com/test.owl#hasValue");
        var object1 = new NamedNode("http://example.com/object1");
        this.object2 = new NamedNode("http://example.com/object2");

        this.builder.addTriple(this.subject, this.predicate, object1)
          .addTriple(this.subject, this.predicate, this.object2);
      });

      it("will remove the input triple to the attached graph (strings)", function () {
        this.builder.removeTriple("<http://example.com/subject>", "<http://example.com/test.owl#hasValue>", "<http://example.com/object2>");

        assert.strictEqual(this.builder.graph.length, 1, "One Triple in graph");
        assert.strictEqual(this.builder.graph.match("<http://example.com/subject>", "<http://example.com/test.owl#hasValue>", "<http://example.com/object1>").length, 1, "It was the expected Triple");
      });

      it("will remove the input triple to the attached graph (nodes)", function () {
        this.builder.removeTriple(this.subject, this.predicate, this.object2);

        assert.strictEqual(this.builder.graph.length, 1, "One Triple in graph");
        assert.strictEqual(this.builder.graph.match("<http://example.com/subject>", "<http://example.com/test.owl#hasValue>", "<http://example.com/object1>").length, 1, "It was the expected Triple");
      });
    });

    describe("method - remove", function () {
      beforeEach(function () {
        this.predicate = new NamedNode("http://example.com/test.owl#hasValue");
        var object1 = new NamedNode("http://example.com/object1");
        this.object2 = new NamedNode("http://example.com/object2");

        this.builder.add(this.predicate, object1)
          .add(this.predicate, this.object2);
      });

      it("will remove the input triple to the attached graph (strings)", function () {
        var subject = this.builder.subject.toNT();
        this.builder.remove("<http://example.com/test.owl#hasValue>", "<http://example.com/object2>");

        assert.strictEqual(this.builder.graph.length, 1, "One Triple in graph");
        assert.strictEqual(this.builder.graph.match(subject, "<http://example.com/test.owl#hasValue>", "<http://example.com/object1>").length, 1, "It was the expected Triple");
      });

      it("will remove the input triple to the attached graph (nodes)", function () {
        var subject = this.builder.subject.toNT();
        this.builder.remove(this.predicate, this.object2);

        assert.strictEqual(this.builder.graph.length, 1, "One Triple in graph");
        assert.strictEqual(this.builder.graph.match(subject, "<http://example.com/test.owl#hasValue>", "<http://example.com/object1>").length, 1, "It was the expected Triple");
      });
    });

    describe("method - removeAll", function () {
      beforeEach(function () {
        var subject = this.builder.subject;
        this.predicate1 = new NamedNode("http://example.com/test.owl#hasValue");
        var predicate2 = new NamedNode("http://example.com/test.owl#hasChildren");
        var object1 = new NamedNode("http://example.com/object1");
        var object2 = new NamedNode("http://example.com/object2");
        var object3 = new NamedNode("http://example.com/object3");

        this.builder.addTriple(subject, this.predicate1, object1)
          .addTriple(subject, this.predicate1, object2)
          .addTriple(subject, predicate2, object2)
          .addTriple(subject, predicate2, object3);
      });

      it("will remove all triples from for the input predicate from the attached graph (strings)", function () {
        this.builder.removeAll("<http://example.com/test.owl#hasValue>");

        assert.strictEqual(this.builder.graph.length, 2, "Two Triples left in graph");
        assert.strictEqual(this.builder.graph.match("<http://example.com/subject>", "<http://example.com/test.owl#hasValue>", null).length, 0, "remaining triples were not associated with predicate");
      });

      it("will remove all triples from for the input predicate from the attached graph (nodes)", function () {
        this.builder.removeAll(this.predicate1);

        assert.strictEqual(this.builder.graph.length, 2, "Two Triples left in graph");
        assert.strictEqual(this.builder.graph.match("<http://example.com/subject>", "<http://example.com/test.owl#hasValue>", null).length, 0, "remaining triples were not associated with predicate");
      });
    });

    describe("method - add", function () {
      it("will add the a triple for the predicate and object to the attached graph (strings)", function () {
        this.builder.add("<http://example.com/test.owl#hasValue>", "<http://example.com/object>");

        assert.strictEqual(this.builder.graph.length, 1, "One Triple in graph");
        assert.strictEqual(this.builder.graph.match(this.builder.subject.toNT(), "<http://example.com/test.owl#hasValue>", "<http://example.com/object>").length, 1, "It was the expected Triple");
      });

      it("will add the a triple for the predicate and object to the attached graph (nodes)", function () {
        var predicate = new NamedNode("http://example.com/test.owl#hasValue");
        var object = new NamedNode("http://example.com/object");
        this.builder.add(predicate, object);

        assert.strictEqual(this.builder.graph.length, 1, "One Triple in graph");
        assert.strictEqual(this.builder.graph.match(this.builder.subject.toNT(), "<http://example.com/test.owl#hasValue>", "<http://example.com/object>").length, 1, "It was the expected Triple");
      });
    });

    describe("method - set", function () {
      beforeEach(function () {
        this.predicate1 = new NamedNode("http://example.com/test.owl#hasValue");
        var predicate2 = new NamedNode("http://example.com/test.owl#hasChildren");
        var object1 = new NamedNode("http://example.com/object1");
        var object2 = new NamedNode("http://example.com/object2");
        var object3 = new NamedNode("http://example.com/object3");

        this.builder.add(this.predicate1, object1)
          .add(this.predicate1, object2)
          .add(predicate2, object2)
          .add(predicate2, object3);
      });

      it("will remove all triples for the input predicate and add the object from the attached graph (strings)", function () {
        var subject = this.builder.subject.toNT();
        var predicate = "<http://example.com/test.owl#hasValue>";
        this.builder.set(predicate, "<http://example.com/object4>");

        assert.strictEqual(this.builder.graph.length, 3, "Two Triples left in graph");
        assert.strictEqual(this.builder.graph.match(subject, predicate, null).length, 1, "remaining triples were not associated with predicate");
        assert.strictEqual(this.builder.graph.match(subject, predicate, "<http://example.com/object4>").length, 1, "remaining triples were not associated with predicate");
      });

      it("will remove all triples for the input predicate and add the object from the attached graph (nodes)", function () {
        var subject = this.builder.subject.toNT();
        var predicate = this.predicate1.toNT();
        var object4 = new NamedNode("http://example.com/object4");
        this.builder.set(this.predicate1, object4);

        assert.strictEqual(this.builder.graph.length, 3, "Two Triples left in graph");
        assert.strictEqual(this.builder.graph.match(subject, predicate, null).length, 1, "remaining triples were not associated with predicate");
        assert.strictEqual(this.builder.graph.match(subject, predicate, object4.toNT()).length, 1, "remaining triples were not associated with predicate");
      });
    });

    describe("method - addRdfType", function () {
      it("will add the desired type to the graph", function () {
        this.builder.addType("<http://example.com/test.owl#Type>");

        assert.strictEqual(this.builder.graph.match(this.builder.subject.toNT(), rdf("type").toNT(), "<http://example.com/test.owl#Type>").length, 1);
      });
    });

    describe("method - removeRdfType", function () {
      it("will remove the desired type to the graph", function () {
        var type1 = "<http://example.com/test.owl#Type1>";
        var type2 = "<http://example.com/test.owl#Type2>";
        this.builder.addType(type1).addType(type2);

        this.builder.removeType(type2);

        assert.strictEqual(this.builder.graph.match(this.builder.subject.toNT(), rdf("type").toNT(), type1).length, 1);
        assert.strictEqual(this.builder.graph.match(this.builder.subject.toNT(), rdf("type").toNT(), type2).length, 0);
      });
    });
  });
});
