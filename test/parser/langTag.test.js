/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import langTag from "../../src/parser/langTag";

import Data from "../fake/blocks/parser/Data";

describe("package - RdfJs", function () {
  describe("package - parser", function () {
    describe("function - langTag", function () {
      it("will parse a simple two digit language tag", function () {
        var data = new Data({
          input: "@en@fr@es Done"
        });

        assert.strictEqual("en", langTag(data));
        assert.strictEqual("fr", langTag(data));
        assert.strictEqual("es", langTag(data));

        assert.isNull(langTag(data));
      });

      it("will parse a complex language tag with '-'", function () {
        var data = new Data({
          input: "@es-mx@en-uk@es-005 Done"
        });

        assert.strictEqual("es-mx", langTag(data));
        assert.strictEqual("en-uk", langTag(data));
        assert.strictEqual("es-005", langTag(data));

        assert.isNull(langTag(data));
      });
    });
  });
});
