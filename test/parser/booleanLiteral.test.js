/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import booleanLiteral from "../../src/parser/boolean";
import Data from '../fake/blocks/parser/Data';
import isLiteral from "../api/node/isLiteral";

describe("package - RdfJs", function () {
  describe("package - parser", function () {
    describe("method - booleanLiteral", function () {
      it("will return null if 'true' or 'false' key words are not present", function () {
        var data = new Data({
          input: "{ ?s ?p ?o }"
        });

        var out = booleanLiteral(data);

        assert.isNull(out);
      });

      it("will return xsd:boolean Literal Node when 'true' appears in string", function () {
        var data = new Data({
          input: "true asdf"
        });

        var out = booleanLiteral(data);

        isLiteral(out, "booleanLiteral should be a Literal Node");
        assert.strictEqual(out.toNT(), '"true"^^<http://www.w3.org/2001/XMLSchema#boolean>');
        assert.strictEqual(4, data.pos);
      });

      it("will return xsd:boolean Literal Node when 'false' appears in string", function () {
        var data = new Data({
          input: "false asdf"
        });

        var out = booleanLiteral(data);

        isLiteral(out, "booleanLiteral should be a Literal Node");
        assert.strictEqual(out.toNT(), '"false"^^<http://www.w3.org/2001/XMLSchema#boolean>');
        assert.strictEqual(5, data.pos);
      });
    });
  });
});
