/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import numeric from "../../src/parser/numeric";
import Data from "../fake/blocks/parser/Data";
import isLiteral from "../api/node/isLiteral";


describe("package - RdfJs", function () {
  describe("package - parser", function () {
    describe("function - numeric", function () {
      it("will return null if input is not a numeric", function () {
        var data = new Data({
          input: "Not an numeric"
        });

        assert.isNull(numeric(data));
      });

      it("will return numeric (double)", function () {
        var data = new Data({
          input: "1029384756.0987654321 3"
        });

        var rtn = numeric(data);

        isLiteral(rtn, "Expected Literal Node return");
        assert.isNotNull(rtn.datatype);
        assert.strictEqual(rtn.datatype.toNT(), "<http://www.w3.org/2001/XMLSchema#decimal>");
        assert.strictEqual(1029384756.0987654321, rtn.valueOf());

        assert.strictEqual(' ', data.input[ data.pos ]);
      });

      it("will return numeric when given decimal without whole component", function () {
        var data = new Data({
          input: ".0987654321 3"
        });

        var rtn = numeric(data);
        isLiteral(rtn, "Expected Literal Node return");
        assert.isNotNull(rtn.datatype);
        assert.strictEqual(rtn.datatype.toNT(), "<http://www.w3.org/2001/XMLSchema#decimal>");
        assert.strictEqual(0.0987654321, rtn.valueOf());

        assert.strictEqual(' ', data.input[ data.pos ]);
      });

      it("will return an integer if number without fraction digits", function () {
        var data = new Data({
          input: "1029384756. 3"
        });

        var rtn = numeric(data);
        isLiteral(rtn, "Expected Literal Node return");
        assert.strictEqual(rtn.datatype.toNT(), "<http://www.w3.org/2001/XMLSchema#integer>");
        assert.strictEqual(1029384756, rtn.valueOf());

        assert.strictEqual('.', data.input[ data.pos ]);
      });

      it("will return an integer on numeric value w/o fraction digits", function () {
        var data = new Data({
          input: "1029384756 3"
        });

        var rtn = numeric(data);
        isLiteral(rtn, "Expected Literal Node return");
        assert.strictEqual(rtn.datatype.toNT(), "<http://www.w3.org/2001/XMLSchema#integer>");
        assert.strictEqual(1029384756, rtn.valueOf());

        assert.strictEqual(' ', data.input[ data.pos ]);
      });

      it("will turn a double if value contains exponent", function () {
        var data = new Data({
          input: "1.e2 3e-4 .5e6 7.8e9"
        });

        var rtn = numeric(data);
        isLiteral(rtn, "Expected Literal Node return");
        assert.strictEqual(rtn.datatype.toNT(), "<http://www.w3.org/2001/XMLSchema#double>");
        assert.strictEqual(1e2, rtn.valueOf());
        data.pos++;
        assert.strictEqual(3e-4, numeric(data).valueOf());
        data.pos++;
        assert.strictEqual(0.5e6, numeric(data).valueOf());
        data.pos++;
        assert.strictEqual(7.8e9, numeric(data).valueOf());
      });
    });
  });
});
