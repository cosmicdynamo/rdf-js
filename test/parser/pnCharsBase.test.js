/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import pnCharsBase from "../../src/parser/pnCharsBase";
import Data from "../fake/blocks/parser/Data";

describe("package - RdfJs", function () {
  describe("package - parser", function () {
    describe("function - phCharsBase", function () {
      it("will handle lower-case alpha chars", function () {
        var input = "";
        var char = "a";
        while (char <= "z") {
          input += char;
          char++;
        }
        var data = new Data({ input });

        for (var idx = 0; idx < data.input.length; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle upper-case alpha chars", function () {
        var input = "";
        var char = "A";
        while (char <= "Z") {
          input += char;
          char++;
        }
        var data = new Data({ input });

        for (var idx = 0; idx < data.input.length; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0x00C0-0x00D6", function () {
        var data = new Data({ input: "" });
        var char = 0x00C0;
        while (char <= 0x00D6) {
          data.input += String.fromCharCode(char);
          char++;
        }
        data.input += String.fromCharCode(0x00D7);
        data.input += String.fromCharCode(0x00BF);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0x00C0-0x00D6", function () {
        var data = new Data({ input: "" });
        var char = 0x00D8;
        while (char <= 0x00F6) {
          data.input += String.fromCharCode(char);
          char++;
        }
        data.input += String.fromCharCode(0x00F7);
        data.input += String.fromCharCode(0x00D7);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0x00F8-0x02FF", function () {
        var data = new Data({ input: "" });
        var char = 0x00F8;
        while (char <= 0x02FF) {
          data.input += String.fromCharCode(char);
          char++;
        }
        data.input += String.fromCharCode(0x0300);
        data.input += String.fromCharCode(0x00F7);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0x0370-0x037D", function () {
        var data = new Data({ input: "" });
        var char = 0x0370;
        while (char <= 0x037D) {
          data.input += String.fromCharCode(char);
          char++;
        }
        data.input += String.fromCharCode(0x037E);
        data.input += String.fromCharCode(0x036F);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0x037F-0x1FFF", function () {
        var data = new Data({ input: "" });
        var char = 0x037F;
        while (char <= 0x1FFF) {
          data.input += String.fromCharCode(char);
          char += 2;
        }
        data.input += String.fromCharCode(0x2000);
        data.input += String.fromCharCode(0x037E);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0x200C-0x200D", function () {
        var data = new Data({ input: "" });
        var char = 0x200C;
        while (char <= 0x200D) {
          data.input += String.fromCharCode(char);
          char++;
        }
        data.input += String.fromCharCode(0x200E);
        data.input += String.fromCharCode(0x200B);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0x2070-0x218F", function () {
        var data = new Data({ input: "" });
        var char = 0x2070;
        while (char <= 0x218F) {
          data.input += String.fromCharCode(char);
          char++;
        }
        data.input += String.fromCharCode(0x2190);
        data.input += String.fromCharCode(0x206F);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0x2C00-0x2FEF", function () {
        var data = new Data({ input: "" });
        var char = 0x2C00;
        while (char <= 0x2FEF) {
          data.input += String.fromCharCode(char);
          char++;
        }
        data.input += String.fromCharCode(0x2FF0);
        data.input += String.fromCharCode(0x2BFF);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0x3001-0xD7FF", function () {
        var data = new Data({ input: "" });
        var char = 0x3001;
        while (char <= 0xD7FF) {
          data.input += String.fromCharCode(char);
          char += 4;
        }
        data.input += String.fromCharCode(0xD800);
        data.input += String.fromCharCode(0x3000);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0xF900-0xFDCF", function () {
        var data = new Data({ input: "" });
        var char = 0xF900;
        while (char <= 0xFDCF) {
          data.input += String.fromCharCode(char);
          char += 2;
        }
        data.input += String.fromCharCode(0xFDD0);
        data.input += String.fromCharCode(0xF8FF);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0xFDF0-0xFFFD", function () {
        var data = new Data({ input: "" });
        var char = 0xFDF0;
        while (char <= 0xFFFD) {
          data.input += String.fromCharCode(char);
          char++;
        }
        data.input += String.fromCharCode(0xFFFE);
        data.input += String.fromCharCode(0xFDEF);

        for (var idx = 0; idx < data.input.length - 2; idx++) {
          assert.strictEqual(data.input[ idx ], pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });

      it("will handle 0x10000-0xEFFFF", function () {
        var data = new Data({ input: "" });
        var char = 0x10000;
        while (char <= 0xEFFFF) {
          var value = char - 0x10000;
          data.input += String.fromCharCode(((value >>> 10) & 0x3FF) | 0xD800);
          value = 0xDC00 | (value & 0x3FF);

          data.input += String.fromCharCode(value);

          char += 100;
        }

        char = 0xF0000;
        value = char - 0x10000;
        data.input += String.fromCharCode(((value >>> 10) & 0x3FF) | 0xD800);
        value = 0xDC00 | (value & 0x3FF);
        data.input += String.fromCharCode(value);

        data.input += String.fromCharCode(0xFFFF);

        for (var idx = 0; idx < data.input.length - 3; idx += 2) {
          assert.strictEqual(data.input.substr(idx, 2), pnCharsBase(data));
        }

        assert.isNull(pnCharsBase(data), "Upper Bounds Test (2ch ASCII)");
        data.pos++;
        assert.isNull(pnCharsBase(data), "Upper Bounds Test (2ch ASCII)");
        data.pos++;

        assert.isNull(pnCharsBase(data), "Lower Bounds Test");
        data.pos++;

        assert.isTrue(data.isEnd(), "Progressed to the end of the string");
      });
    });
  });
});
