/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import rdfType from "../../src/parser/rdfType";
import Data from "../fake/blocks/parser/Data";
import isNamed from "../api/node/isNamed";

describe("package - RdfJs", function () {
  describe("package - parser", function () {
    describe("function - rdfType", function () {
      it("will return RdfJs.Node for rdf:type if next word is 'a'", function () {
        var data = new Data({
          input: "a"
        });

        var out = rdfType(data);

        isNamed(out, "rdfType should return a Named Node");
        assert.strictEqual("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>", out.toNT());
        assert.strictEqual(1, data.pos, "position incremented");
      });

      it("will strip leading white space", function () {
        var data = new Data({
          input: " a",
          whiteSpace: function (token) {
            token.pos++;
          }
        });

        var out = rdfType(data);

        assert.strictEqual("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>", out.toNT());
        assert.isTrue(data.isEnd(), "position incremented");
      });

      it("returns null if next char is not 'a'", function () {
        var data = new Data({
          input: "fail"
        });

        var out = rdfType(data);

        assert.isNull(out);
        assert.strictEqual(0, data.pos, "position not moved");
      });

      it("is case-sensitive and returns null even if next char is 'A'", function () {
        var data = new Data({
          input: "A"
        });

        var out = rdfType(data);

        assert.isNull(out);
        assert.strictEqual(0, data.pos, "position not moved");
      });
    });
  });
});
