/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import nil from "../../src/parser/nil";
import Data from "../fake/blocks/parser/Data";
import isNamed from "../api/node/isNamed";

describe("package - RdfJs", function () {
  describe("package - parser", function () {
    describe("function - nil", function () {
      it("Will pull empty '(', ')' w/ whiteSpace", function () {
        var data = new Data({
          input: "(   )",
          whiteSpace: token => {
            if (token.input[ token.pos ] === ' ') {
              token.pos++;
              return 1;
            }
            return null;
          }
        });

        var out = nil(data);

        isNamed(out, "expected RDF Named Node");
        assert.strictEqual(out.toNT(), "<http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>");
      });

      it("Will pull empty '(', ')' w/o whiteSpace", function () {
        var data = new Data({
          input: "()",
          whiteSpace: function (token) {
            if (token.input[ token.pos ] === ' ') {
              token.pos++;
              return 1;
            }
            return null;
          }
        });

        var out = nil(data);

        isNamed(out, "expected RDF Named Node");
        assert.strictEqual(out.toNT(), "<http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>");
      });

      it("will return null if missing open parenthesis", function () {
        var data = new Data({
          input: ")",
          whiteSpace: function (token) {
            if (token.input[ token.pos ] === ' ') {
              token.pos++;
              return 1;
            }
            return null;
          }
        });

        var out = nil(data);
        assert.isNull(out);
      });

      it("will return null if missing close parenthesis", function () {
        var data = new Data({
          input: "(",
          whiteSpace: function (token) {
            if (token.input[ token.pos ] === ' ') {
              token.pos++;
              return 1;
            }
            return null;
          }
        });

        var out = nil(data);
        assert.isNull(out);
      });
    });
  });
});
