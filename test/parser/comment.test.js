/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import comment from "../../src/parser/comment";
import Data from "../fake/blocks/parser/Data";

describe("package - RdfJs", function () {
  describe("package - parser", function () {
    describe("function - comment", function () {
      it("will move pointer to end of line if '#' is next character", function () {
        var data = new Data({
          input: "#This is an RDF '#' comment <So There >'"
        });

        var out = comment(data);

        assert.strictEqual("#", out);
        assert.strictEqual(data.input.length, data.pos);
      });

      it("will stop if next character is not '#'", function () {
        var data = new Data({
          input: " #This is an RDF '#' comment <So There >'\nSome More Text"
        });

        var out = comment(data);

        assert.isNull(out);
        assert.strictEqual(0, data.pos);
      });

      it("will stop after a new line is reached", function () {
        var data = new Data({
          input: "#This is an RDF '#' comment <So There >'\nSome More Text"
        });

        var out = comment(data);

        assert.strictEqual("#", out);
        assert.strictEqual("Some More Text", data.input.substr(data.pos));
      });
    });
  });
});
