/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Compares to RDF Graphs
 * @param {RdfJs.Graph} left - a graph to compare
 * @param {RdfJs.Graph} right - a graph to compare against
 * @returns {boolean} - true if the two graphs contain all the same triples
 */
export default function (left, right) {
  if (left.length !== right.length) {
    return false;
  }

  var l = left.clone();
  var r = right.clone();

  let pass = l.every(triple => {
    if (triple.subject.isBlank() || triple.object.isBlank()) {
      return true;
    }
    var has = r.has(triple);

    if (has) {
      r.remove(triple);
      l.remove(triple);
    }
    return has;
  });
  if (!pass) {
    return false;
  }

  if (l.length !== r.length) {
    return false;
  }

  var hash = function (t) {
    var arr = [ t.subject.isBlank() ? "~" : t.subject.toNT() ];
    arr.push(t.predicate.toNT());
    arr.push([ t.object.isBlank() ? "~" : t.object.toNT() ]);
    t.hash = arr.join(" ");
  };

  var lArray = l.toArray();
  lArray.forEach(hash);
  var rArray = r.toArray();
  rArray.forEach(hash);

  var sortFn = function (v1, v2) {
    return (v1.hash < v2.hash) ? -1 : 1;
  };

  lArray.sort(sortFn);
  rArray.sort(sortFn);

  var nHashL = {};
  var nHashR = {};
  var hash2 = function (t, h) {
    if (t.subject.isBlank()) {
      h[ t.subject.toNT() ] = (h[ t.subject.toNT() ] || "") + t.hash;
    }

    if (t.object.isBlank()) {
      h[ t.object.toNT() ] = t.hash + (h[ t.object.toNT() ] || "");
    }
  };

  for (var idx = 0; idx < lArray.length; idx++) {
    if (lArray[ idx ].hash !== rArray[ idx ].hash) {
      return false;
    }

    hash2(lArray[ idx ], nHashL);
    hash2(rArray[ idx ], nHashR);
  }

  var lKeys = Object.keys(nHashL);
  var rKeys = Object.keys(nHashR);

  if (lKeys.length !== rKeys.length) {
    return false;
  }

  for (idx = 0; idx < lKeys.length; idx++) {
    if (nHashL[ lKeys[ idx ] ] !== nHashR[ rKeys[ idx ] ]) {
      return false;
    }
  }

  return true;
}
