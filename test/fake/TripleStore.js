/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { TripleStore, typeTest } from "../../src";
import isTriple from "../api/isTriple";

/**
 * @class RdfJs.test.fake.TripleStore
 * @mixes qasht._Fake
 * @mixes dojo.declare
 * @mixes RdfJs.TripleStore
 */
class FakeTripleStore extends TripleStore {
  constructor(args) {
    let options = args || {};
    if (options.GraphCtor) {
      assert.isFunction(options.GraphCtor, "GraphCtor must be a function");
    }
    if (options.default) {
      assert.isString(options.default, "default must be a String");
    }
    super(options);
  }

  addGraph(name, data) {
    assert.isString(name, "TripleStore.addGraph: name must be a String");

    if (data) {
      assert.isObject(data, "TripleStore.addGraph: 2nd parameter (options) MUST be an RDF Graph");
    }

    return super.addGraph(name, data);
  }

  runOnGraphs(method, graphs, create) {
    var ct = arguments.length;
    assert.isTrue(ct > 0 && ct < 4, "TripleStore.runOnGraphs: takes one - three argument");

    let graphArray = graphs || "DEFAULT";
    if (!typeTest.isArray(graphArray)) {
      graphArray = [ graphArray ];
    }

    assert.isFunction(method, "TripleStore.runOnGraphs: method MUST be a Function");
    graphArray.forEach(function (graph) {
      assert.isString(graph, "TripleStore.runOnGraphs: graph MUST be a String | String[]");
    });
    if (create !== undefined) { // eslint-disable-line no-undefined
      assert.isTrue(create === true || create === false, "TripleStore.runOnGraphs: Optional create MUST be a Boolean");
    }
    return super.runOnGraphs(method, graphArray, create);
  }

  setDefault(value) {
    assert.stricttEqual(arguments.length, 1, "TripleStore.setDefault: takes one argument");
    let asArray = typeTest.isArray(value) ? value : [ value ];

    asArray.forEach(function (graph) {
      assert.isString(graph, "TripleStore.setDefault: value MUST be a String | String[]");
    });
    return super.setDefault(asArray);
  }

  add(triple, graphName) {
    assert.strictEqual(arguments.length, 2, "TripleStore.add: takes two arguments");
    isTriple(triple);

    return super.add(triple, graphName);
  }

  addAll(graph, graphName) {
    assert.strictEqual(arguments.length, 2, "TripleStore.addAll: takes two arguments");

    assert.isFunction(graph.forEach, "TripleStore.addAll: graph must have a forEach method");
    return super.addAll(graph, graphName);
  }

  remove(triple, graphName) {
    assert.strictEqual(arguments.length, 2, "TripleStore.remove: takes two arguments");
    isTriple(triple);

    return super.remove(triple, graphName);
  }

  removeMatches(subject, predicate, object, graphName) {
    var ct = arguments.length;
    assert.isTrue(ct > 2 && ct < 5, "TripleStore.runOnGraphs: takes three or four argument");
    if (subject) {
      assert.isString(subject, "TripleStore.removeMatches: subject MUST be a String");
    }
    if (predicate) {
      assert.isString(predicate, "TripleStore.removeMatches: predicate MUST be a String");
    }
    if (object) {
      assert.isString(object, "TripleStore.removeMatches: object MUST be a String");
    }


    return super.removeMatches(subject, predicate, object, graphName);
  }

  toArray(graphName) {
    assert.strictEqual(arguments.length, 1, "TripleStore.remove: takes one arguments");

    return super.toArray(graphName);
  }

  some(tFilter, graphName) {
    assert.strictEqual(arguments.length, 2, "TripleStore.some: takes two arguments");
    assert.isFunction(tFilter);

    return super.some(tFilter, graphName);
  }

  every(tFilter, graphName) {
    assert.strictEqual(arguments.length, 2, "TripleStore.every: takes two arguments");
    assert.isFunction(tFilter);

    return super.every(tFilter, graphName);
  }

  filter(tFilter, graphName) {
    assert.strictEqual(arguments.length, 2, "TripleStore.filter: takes two arguments");
    assert.isFunction(tFilter);

    return super.filter(tFilter, graphName);
  }

  forEach(tCallback, graphName) {
    assert.strictEqual(arguments.length, 2, "TripleStore.forEach: takes two arguments");
    assert.isFunction(tCallback);

    return super.forEach(tCallback, graphName);
  }

  match(subject, predicate, object, graphName) {
    var ct = arguments.length;
    assert.isTrue(ct > 2 && ct < 5, "TripleStore.match: takes three or four argument");
    if (subject) {
      assert.isString(subject, "TripleStore.match: subject MUST be a String");
    }
    if (predicate) {
      assert.isString(predicate, "TripleStore.match: predicate MUST be a String");
    }
    if (object) {
      assert.isString(object, "TripleStore.match: object MUST be a String");
    }


    return super.match(subject, predicate, object, graphName);
  }

  getGraph(name) {
    assert.isString(name, "TripleStore.getGraph: name MUST be a String");

    return super.getGraph(name);
  }
}

export default FakeTripleStore;
