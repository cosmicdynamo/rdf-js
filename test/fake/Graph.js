/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { Graph } from "../../src";
import FakeTriple from "./Triple";
import isGraph from "../api/isGraph";

/**
 * @class RdfJs.test.fake.Graph
 * @mixes qasht._Fake
 * @mixes RdfJs.Graph
 * @mixes dojo._base.declare
 */
class FakeGraph extends Graph {
  add(triple) {
    assert.strictEqual(arguments.length, 1, "Graph.add requires one parameter");
    FakeTriple.testApi(triple, "Graph.add, 1st param MUST be a triple");

    return super.add(triple);
  }

  remove(triple) {
    assert.strictEqual(arguments.length, 1, "Graph.remove requires one parameter");
    FakeTriple.testApi(triple, "Graph.remove 1st parameter MUST be a triple");

    return super.remove(triple);
  }

  removeMatches(s, p, o) {
    assert.strictEqual(3, arguments.length, "Graph.removeMatches requires three parameters");
    if (s !== null) {
      assert.isString(s, "Graph.removeMatches - subject MUST be a string");
    }
    if (p !== null) {
      assert.isString(p, "Graph.removeMatches - predicate MUST be a string");
    }
    if (o !== null) {
      assert.isString(o, "Graph.removeMatches - object MUST be a string");
    }

    return super.removeMatches(s, p, o);
  }

  toArray() {
    assert.strictEqual(arguments.length, 0, "Graph.toArray does not take any parameters");

    return super.toArray();
  }

  some(tFilter) {
    assert.strictEqual(arguments.length, 1, "Graph.some requires 1 parameter");
    assert.isFunction(tFilter, "Graph.some parameter MUST be a function");

    return super.some(tFilter);
  }

  every(tFilter) {
    assert.strictEqual(arguments.length, 1, "Graph.every takes one parameter");
    assert.isFunction(tFilter, "Graph.every parameter MUST be a function");

    return super.every(tFilter);
  }

  filter(tFilter) {
    assert.strictEqual(arguments.length, 1, "Graph.filter takes one parameter");
    assert.isFunction(tFilter, "Graph.filter parameter MUST be a function");

    return super.filter(tFilter);
  }

  forEach(tCallback) {
    assert.strictEqual(arguments.length, 1, "Graph.forEach takes one parameter");
    assert.isFunction(tCallback, "Graph.forEach parameter MUST be a function");

    return super.forEach(tCallback);
  }

  match(subject, predicate, object, limit) {
    assert.isTrue(arguments.length === 3 || arguments.length === 4, "Graphy.match takes between 3 and 4 arguments");
    if (subject !== null) {
      assert.isString(subject, "Graph.match - subject MUST be a string or NULL");
    }
    if (predicate !== null) {
      assert.isString(predicate, "Graph.match - predicate MUST be a string or NULL");
    }
    if (object !== null) {
      assert.isString(object, "Graph.match - object MUST be a string or NULL");
    }
    if (limit !== undefined) { // eslint-disable-line no-undefined
      assert.isNumber(limit, "limit is a number");
      assert.notEqual(limit, 0, "Limit is not 0");
    }

    return super.match(subject, predicate, object, limit);
  }

  merge(graph) {
    assert.strictEqual(arguments.length, 1, "Graph.merge takes one argument");
    isGraph(graph);

    return super.merge(graph);
  }

  addAll(graph) {
    assert.strictEqual(arguments.length, 1, "Graph.addAll takes one argument");
    isGraph(graph);

    return super.addAll(graph);
  }

  //noinspection JSUnusedGlobalSymbols
  onChange() {
    assert.strictEqual(arguments.length, 0, "Graph.onChage does not take any arguments");

    return super.onChange();
  }

  // noinspection JSMethodCanBeStatic
  addAction() {
    assert.fail(null, null, "Graph.addAction is deprecated");
  }

  equals(graph) {
    assert.strictEqual(arguments.length, 1, "Graph.equals takes one argument");
    isGraph(graph);

    return super.equals(graph);
  }

  clone() {
    assert.strictEqual(arguments.length, 0, "Graph.clone does not take any arguments");

    return super.clone();
  }

  has(triple) {
    assert.strictEqual(arguments.length, 1, "Graph.has takes one argument");
    FakeTriple.testApi(triple);

    return super.has(triple);
  }

  // noinspection JSUnusedGlobalSymbols
  __addTriple(s, p, o) {
    this.add(new FakeTriple(s, p, o));
  }
}

FakeGraph.testApi = isGraph;

export default FakeGraph;
