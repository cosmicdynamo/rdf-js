/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { Triple } from "../../src";
import Node from "./Node";
import isTriple from "../api/isTriple";

/**
 * @class RdfJs.test.fake.Triple
 * @mixes RdfJs.Triple
 * @mixes qasht._Fake
 */
class FakeTriple extends Triple {
  constructor(subject, predicate, object) {
    super(subject, predicate, object);

    assert.isObject(this.subject, "Triple subject is set");
    assert.isObject(this.predicate, "Triple predicate is set");
    assert.isObject(this.object, "Triple object is set");

    this.subject = new Node(subject.subject || subject);
    this.predicate = new Node(subject.predicate || predicate);
    this.object = new Node(subject.object || object);
  }

  toNT() {
    assert.strictEqual(arguments.length, 0, "Triple.toNT does not take any arguments");

    return this.subject.toNT() + " " + this.predicate.toNT() + " " + this.object.toNT();
  }

  toString() {
    assert.strictEqual(arguments.length, 0, "Triple.toString does not take any arguments");

    return this.subject.toNT() + " " + this.predicate.toNT() + " " + this.object.toNT() + " .";
  }

  equals(t) {
    assert.strictEqual(arguments.length, 1, "Triple.equals takes on arguments");
    isTriple(t);

    return this.subject.equals(t.subject) && this.predicate.equals(t.predicate) && this.object.equals(t.object);
  }
}

/**
 * @method RdfJs.test.fake.Triple#testApi
 * @param {*} object - The object being tested
 */
FakeTriple.testApi = isTriple;

export default FakeTriple;
