/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { Profile } from "../src";

describe("package - RdfJs", function () {
  describe("class - Profile", function () {
    it("it will import the values from the constructor input param", function () {
      var profile = new Profile({
        prefixes: {
          test: "http://example.com/",
          "": "http://example.com/default-prefix#"
        },
        terms: {
          test: "http://example.com/"
        },
        vocab: "http://example.com/default-term#"
      });

      assert.strictEqual(profile.prefixes.get("test"), "http://example.com/");
      assert.strictEqual(profile.prefixes.get(""), "http://example.com/default-prefix#");

      assert.strictEqual(profile.terms.get("test"), "http://example.com/");
      assert.strictEqual(profile.terms.getDefault(), "http://example.com/default-term#");
    });

    describe("method - resolve", function () {
      it("returns the result of calling prefixes.resolve if input contains a ':'", function () {
        var profile = new Profile({
          prefixes: {
            test: "http://example.com/",
            "": "http://example.com/default-prefix#"
          },
          terms: {
            test: "http://example.com/"
          },
          vocab: "http://example.com/default-term#"
        });

        assert.strictEqual(profile.resolve("test:value"), "http://example.com/value");
      });

      it("returns the result of calling terms.resolve if input does not contain a ':'", function () {
        // "http://www.w3.org/TR/rdf-interfaces/#widl-Profile-resolve-DOMString-DOMString-toresolve"
        var profile = new Profile({
          prefixes: {
            test: "http://example.com/",
            "": "http://example.com/default-prefix#"
          },
          terms: {
            test: "http://example.com/"
          },
          vocab: "http://example.com/default-term#"
        });

        assert.strictEqual(profile.resolve("test2"), "http://example.com/default-term#test2");
      });
    });

    describe("method - setVocab", function () {
      it("sets the default prefix to apply if no term matches", function () {
        // "http://www.w3.org/TR/rdf-interfaces/#widl-Profile-setDefaultVocabulary-void-DOMString-iri"
        var profile = new Profile({
          prefixes: {
            test: "http://example.com/",
            "": "http://example.com/default-prefix#"
          },
          terms: {
            test: "http://example.com/"
          },
          vocab: "http://example.com/default-term#"
        });

        assert.strictEqual(profile.resolve("test2"), "http://example.com/default-term#test2");
        profile.setVocab("http://example.org/default#");
        assert.strictEqual(profile.resolve("test2"), "http://example.org/default#test2");
      });
    });

    describe("method - setTerm", function () {
      it("associates an IRI with a term", function () {
        // "http://www.w3.org/TR/rdf-interfaces/#widl-Profile-setTerm-void-DOMString-term-DOMString-iri"
        var profile = new Profile({
          prefixes: {
            test: "http://example.com/",
            "": "http://example.com/default-prefix#"
          },
          terms: {
            test: "http://example.com/"
          },
          vocab: "http://example.com/default-term#"
        });

        assert.strictEqual(profile.resolve("test2"), "http://example.com/default-term#test2");
        profile.setTerm("test2", "http://example.com/test2");
        assert.strictEqual(profile.resolve("test2"), "http://example.com/test2");
      });
    });

    describe("method - setPrefix", function () {
      it("associates an IRI with a prefix", function () {
        // "http://www.w3.org/TR/rdf-interfaces/#widl-Profile-setPrefix-void-DOMString-prefix-DOMString-iri"
        var profile = new Profile({
          prefixes: {
            test: "http://example.com/",
            "": "http://example.com/default-prefix#"
          },
          terms: {
            test: "http://example.com/"
          },
          vocab: "http://example.com/default-term#"
        });

        assert.strictEqual(profile.resolve("test2:"), "test2:");
        profile.setPrefix("test2", "http://example.com/test2");
        assert.strictEqual(profile.resolve("test2:"), "http://example.com/test2");
      });
    });

    describe("method - merge", function () {
      it("adds the terms and prefixes from the input profile into this one", function () {
        // "http://www.w3.org/TR/rdf-interfaces/#widl-Profile-importProfile-Profile-Profile-profile-boolean-override"
        var profile = new Profile({
          prefixes: {
            test: "http://example.com/",
            "": "http://example.com/default-prefix#"
          },
          terms: {
            test: "http://example.com/"
          },
          vocab: "http://example.com/default-term#"
        });

        var more = new Profile({
          prefixes: {
            test: "http://example.com/override",
            test2: "http://example.com/test2",
            "": "http://example.com/default#new"
          },
          terms: {
            test: "http://example.com/override",
            test2: "http://example.com/test2"
          },
          vocab: "http://example.com/default#new"
        });

        profile.merge(more, false);
        assert.strictEqual(profile.resolve("test:"), "http://example.com/");
        assert.strictEqual(profile.resolve("test2:"), "http://example.com/test2");
        assert.strictEqual(profile.resolve(":"), "http://example.com/default-prefix#");

        assert.strictEqual(profile.resolve("test"), "http://example.com/");
        assert.strictEqual(profile.resolve("test2"), "http://example.com/test2");
        assert.strictEqual(profile.resolve("unknown"), "http://example.com/default-term#unknown");
      });

      describe("parameter - override", function () {
        it("will cause input prefixes and terms to override any existing values when set to true", function () {
          // "http://www.w3.org/TR/rdf-interfaces/#widl-Profile-importProfile-Profile-Profile-profile-boolean-override"
          var profile = new Profile({
            prefixes: {
              test: "http://example.com/",
              "": "http://example.com/default-prefix#"
            },
            terms: {
              test: "http://example.com/"
            },
            vocab: "http://example.com/default-term#"
          });

          var more = new Profile({
            prefixes: {
              test: "http://example.com/override",
              test2: "http://example.com/test2",
              "": "http://example.com/default-prefix#new"
            },
            terms: {
              test: "http://example.com/override",
              test2: "http://example.com/test2"
            },
            vocab: "http://example.com/default-term#new"
          });

          profile.merge(more, true);
          assert.strictEqual(profile.resolve("test:"), "http://example.com/override");
          assert.strictEqual(profile.resolve("test2:"), "http://example.com/test2");
          assert.strictEqual(profile.resolve(":"), "http://example.com/default-prefix#new");

          assert.strictEqual(profile.resolve("test"), "http://example.com/override");
          assert.strictEqual(profile.resolve("test2"), "http://example.com/test2");
          assert.strictEqual(profile.resolve("unknown"), "http://example.com/default-term#newunknown");
        });
      });
    });
  });
});
