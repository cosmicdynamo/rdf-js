/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { BlankNode, Literal, NamedNode } from "../../src";

describe("package - RdfJs", function () {
  describe("package - node", function () {
    describe("module - BlankNode", function () {
      it("Will generate a new bNode Id if null or undefined is passed into the constructor", function () {
        var out = new BlankNode();

        assert.isObject(out);
        assert.isString(out.nominalValue);
        assert.isTrue(out.nominalValue.length > 0);
        assert.strictEqual(out.interfaceName, "BlankNode");

        var out2 = new BlankNode();

        assert.notEqual(out.toNT(), out2.toNT());
      });

      it("will create a valid bNode if ctor arg contains _:", function () {
        var out = new BlankNode("_:test");

        assert.isObject(out);
        assert.strictEqual(out.nominalValue, "test");
        assert.strictEqual(out.interfaceName, "BlankNode");
      });

      it("will create a valid bNode if ctor arg is an id without _: prefix", function () {
        var out = new BlankNode("test");

        assert.isObject(out);
        assert.strictEqual(out.nominalValue, "test");
        assert.strictEqual(out.interfaceName, "BlankNode");
      });

      it("will create a valid copy if a bNode is passed to the constructor ", function () {
        var original = new BlankNode("original");
        var copy = new BlankNode(original);

        assert.strictEqual(copy.nominalValue, original.nominalValue, "nominal values match");
        assert.isTrue(original.equals(copy), "Original recognizes the copy is the same");
      });

      describe("toString", function () {
        it("returns the bNode value with '_:' prefix", function () {
          var out = new BlankNode("_:test1");

          assert.strictEqual(out.toString(), "_:test1");

          out = new BlankNode("test2");

          assert.strictEqual(out.toString(), "_:test2");
        });
      });

      describe("valueOf", function () {
        it("will return the nominalValue of the blank node", function () {
          var out = new BlankNode("_:test");

          assert.strictEqual(out.valueOf(), "test", "value of matches the input value");
          assert.strictEqual(out.valueOf(), out.nominalValue, "valueOf matches the nominalValue");
        });
      });

      describe("toNT", function () {
        it("returns the bNode value in NT form", function () {
          var out = new BlankNode("_:test");

          assert.strictEqual(out.toNT(), "_:test");
        });
      });

      describe("equals", function () {
        it("will return false if given another bNode with a different nominal value", function () {
          var out = new BlankNode("_:test");
          var out2 = new BlankNode("_:test2");

          assert.isFalse(out.equals(out2));
        });

        it("will return false when given a Literal node", function () {
          var out = new BlankNode("_:test");
          var out2 = new Literal("test2");

          assert.isFalse(out.equals(out2));
        });

        it("will return false when given a Named node", function () {
          var out = new BlankNode("_:test");
          var out2 = new NamedNode("<test2>");

          assert.isFalse(out.equals(out2));
        });

        it("will return true when given a different blank node with the same nominal value", function () {
          var out = new BlankNode("_:test");
          var out2 = new BlankNode("test");

          assert.isTrue(out.equals(out2));
        });
      });
    });
  });
});
