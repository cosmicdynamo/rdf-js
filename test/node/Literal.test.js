/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { BlankNode, Literal, NamedNode } from "../../src";
import isNamedNode from "../api/node/isNamed";

describe("package - RdfJs", function () {
  describe("package - node", function () {
    describe("module - Literal", function () {
      it("will create a valid literal when provided only a value", function () {
        var out = new Literal("value");

        assert.isObject(out);
        assert.strictEqual(out.interfaceName, "Literal");
        assert.strictEqual(out.nominalValue, "value");
        assert.isNull(out.datatype);
        assert.isNull(out.language);
      });

      it("will create a valid literal when provided a value and a datatype", function () {
        var out = new Literal("value", "http://www.w3.org/2001/XMLSchema#string");

        assert.strictEqual(out.interfaceName, "Literal");
        assert.strictEqual(out.nominalValue, "value");
        isNamedNode(out.datatype, "datatype should be a Named Node");
        assert.strictEqual(out.datatype.toNT(), "<http://www.w3.org/2001/XMLSchema#string>");
        assert.isNull(out.language);
      });

      it("will create a valid literal when provided a value and a Named node for datatype", function () {
        var out = new Literal("value", new NamedNode("http://www.w3.org/2001/XMLSchema#string"));

        assert.strictEqual(out.interfaceName, "Literal");
        assert.strictEqual(out.nominalValue, "value");
        isNamedNode(out.datatype, "datatype is a Named Node");
        assert.strictEqual(out.datatype.toNT(), "<http://www.w3.org/2001/XMLSchema#string>");
        assert.isNull(out.language);
      });

      it("will create a valid literal when provided a value and a languague prefixed with '@'", function () {
        var out = new Literal("value", "@en");

        assert.isObject(out);
        assert.strictEqual(out.interfaceName, "Literal");
        assert.strictEqual(out.nominalValue, "value");
        assert.isNull(out.datatype);
        assert.strictEqual(out.language, "en");
      });

      it("will copy an Existing Literal node if passed to the constructor", function () {
        var originalNoType = new Literal("value");
        var originalTyped = new Literal("value", "http://www.w3.org/2001/XMLSchema#string");
        var originalLang = new Literal("value", "@en");

        var copyNoType = new Literal(originalNoType);
        var copyTyped = new Literal(originalTyped);
        var copyLang = new Literal(originalLang);

        assert.isTrue(originalNoType.equals(copyNoType), "Copied Literal without type information");
        assert.isTrue(originalTyped.equals(copyTyped), "Copied Literal with type information");
        assert.isTrue(originalLang.equals(copyLang), "Copied Literal with language information");
      });

      describe("method - toString", function () {
        it("will return the nominal value as a string", function () {
          var out = new Literal("value");

          assert.strictEqual(out.toString(), "\"value\"");
        });
      });

      describe("method - valueOf", function () {
        it("will return a JavaScript String when the datatype is xsd:String", function () {
          var out1 = new Literal("value");
          var out2 = new Literal("value", "http://www.w3.org/2001/XMLSchema#string");
          var out3 = new Literal("value", "@en");

          assert.strictEqual(out1.valueOf(), "value");
          assert.strictEqual(out2.valueOf(), "value");
          assert.strictEqual(out3.valueOf(), "value");
        });

        it("will return a JavaScript Date for an xsd:data or xsd:dateTime", function () {
          var out = new Literal("2002-05-30T09:30:10-06:00", "http://www.w3.org/2001/XMLSchema#dateTime");
          var out2 = new Literal("2002-05-30", "http://www.w3.org/2001/XMLSchema#date");

          assert.strictEqual(out.valueOf().toString(), (new Date("May 30, 2002 9:30:10 GMT-06:00")).toString());
          assert.strictEqual(out2.valueOf().toString(), (new Date("May 30, 2002 UTC 0")).toString());
        });

        it("will return an integer for xsd:int, xsd:integer, xsd:unsignedInt", function () {
          var out = new Literal("100", "http://www.w3.org/2001/XMLSchema#int");
          var out2 = new Literal("-100", "http://www.w3.org/2001/XMLSchema#integer");
          var out3 = new Literal("127", "http://www.w3.org/2001/XMLSchema#unsignedInt");

          assert.strictEqual(out.valueOf(), 100);
          assert.strictEqual(out2.valueOf(), -100);
          assert.strictEqual(out3.valueOf(), 127);
        });

        it("will return a JavaScript Number for xsd:double, xsd:float, xsd:decimal", function () {
          var out = new Literal("100.12", "http://www.w3.org/2001/XMLSchema#double");
          var out2 = new Literal("100.12654987", "http://www.w3.org/2001/XMLSchema#float");
          var out3 = new Literal("100.12654987", "http://www.w3.org/2001/XMLSchema#decimal");

          assert.strictEqual(out.valueOf(), 100.12);
          assert.strictEqual(out2.valueOf(), 100.12654987);
          assert.strictEqual(out3.valueOf(), 100.12654987);
        });

        it("will return a JavaScript Number for xsd:positiveInteger, and xsd:nonNegativeInteger", function () {
          var out = new Literal("100", "http://www.w3.org/2001/XMLSchema#positiveInteger");
          var out2 = new Literal("127", "http://www.w3.org/2001/XMLSchema#nonNegativeInteger");

          assert.strictEqual(out.valueOf(), 100);
          assert.strictEqual(out2.valueOf(), 127);
        });

        it("will return a JavaScript Number for xsd:negativeInteger, and xsd:nonPositiveInteger", function () {
          var out = new Literal("-100", "http://www.w3.org/2001/XMLSchema#negativeInteger");
          var out2 = new Literal("-100", "http://www.w3.org/2001/XMLSchema#nonPositiveInteger");

          assert.strictEqual(out.valueOf(), -100);
          assert.strictEqual(out2.valueOf(), -100);
        });

        it("will return a JavaScript Number for xsd:long", function () {
          var out = new Literal("987654321", "http://www.w3.org/2001/XMLSchema#long");

          assert.strictEqual(out.valueOf(), 987654321);
        });

        it("will return a JavaScript Number for xsd:short and xsd:unsignedShort", function () {
          var out = new Literal("123", "http://www.w3.org/2001/XMLSchema#short");

          assert.strictEqual(out.valueOf(), 123);
        });

        it("will return a JavaScript Number for xsd:byte", function () {
          var out = new Literal("127", "http://www.w3.org/2001/XMLSchema#byte");

          assert.strictEqual(out.valueOf(), 127);
        });

        it("will return a JavaScript Number for xsd:unsignedLong", function () {
          var out = new Literal("127", "http://www.w3.org/2001/XMLSchema#unsignedLong");

          assert.strictEqual(out.valueOf(), 127);
        });

        it("will return a JavaScript Number for xsd:unsignedShort", function () {
          var out = new Literal("250", "http://www.w3.org/2001/XMLSchema#unsignedShort");

          assert.strictEqual(out.valueOf(), 250);
        });

        it("will return a JavaScript Number for xsd:unsignedByte", function () {
          var out = new Literal("250", "http://www.w3.org/2001/XMLSchema#unsignedByte");

          assert.strictEqual(out.valueOf(), 250);
        });
      });

      describe("method - toNT", function () {
        it("returns the NT form of the Literal Node", function () {
          var out1 = new Literal("string");
          var out2 = new Literal("string", "http://www.w3.org/2001/XMLSchema#string");
          var out3 = new Literal("string", "@en");

          assert.strictEqual(out1.toNT(), "\"string\"");
          assert.strictEqual(out2.toNT(), "\"string\"^^<http://www.w3.org/2001/XMLSchema#string>");
          assert.strictEqual(out3.toNT(), "\"string\"@en");
        });
      });

      describe("equals(toCompare)", function () {
        it("returns false when given a Literal with a different nominalValue", function () {
          var out = new Literal("string");
          var out2 = new Literal("string2");

          assert.isFalse(out.equals(out2));
          assert.isFalse(out2.equals(out));
        });

        it("returns false when given a Literal with a missing datatype", function () {
          var out = new Literal("string", "http://www.w3.org/2001/XMLSchema#string");
          var out2 = new Literal("string");

          assert.isFalse(out.equals(out2));
          assert.isFalse(out2.equals(out));
        });

        it("returns false when given a Literal with a missing language", function () {
          var out = new Literal("string", "@en");
          var out2 = new Literal("string");

          assert.isFalse(out.equals(out2));
          assert.isFalse(out2.equals(out));
        });

        it("returns false when given a Literal with a different datatype", function () {
          var out = new Literal("true", "http://www.w3.org/2001/XMLSchema#string");
          var out2 = new Literal("true", "http://www.w3.org/2001/XMLSchema#boolean");

          assert.isFalse(out.equals(out2));
        });
        it("returns false when given a Literal with a different language", function () {
          var out = new Literal("string", "@en");
          var out2 = new Literal("string", "@fr");

          assert.isFalse(out.equals(out2));
        });
        it("returns false when given a Literal with a different datatype/language", function () {
          var out = new Literal("string", "http://www.w3.org/2001/XMLSchema#string");
          var out2 = new Literal("string", "@en");

          assert.isFalse(out.equals(out2));
          assert.isFalse(out2.equals(out));
        });

        it("returns false when given a Named Node", function () {
          var out = new Literal("string");
          var out2 = new NamedNode("<string>");

          assert.isFalse(out.equals(out2));
        });

        it("returns false when given a Blank Node", function () {
          var out = new Literal("string");
          var out2 = new BlankNode("_:string");

          assert.isFalse(out.equals(out2));
        });

        it("returns true when given a Literal Node with the same nominalValue", function () {
          var out = new Literal("string");
          var out2 = new Literal("string");

          assert.isTrue(out.equals(out2));
        });

        it("returns true when given a Literal Node with the same datatype", function () {
          var out = new Literal("string", "http://www.w3.org/2001/XMLSchema#string");
          var out2 = new Literal("string", "http://www.w3.org/2001/XMLSchema#string");

          assert.isTrue(out.equals(out2));
        });

        it("returns true when given a Literal Node with the same language", function () {
          var out = new Literal("string", "@en");
          var out2 = new Literal("string", "@en");

          assert.isTrue(out.equals(out2));
        });
      });
    });
  });
});
