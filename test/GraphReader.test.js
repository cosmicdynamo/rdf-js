/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Triple from "./fake/Triple";
import { BlankNode, Graph, GraphReader, NamedNode } from "../src";

describe("package - RdfJs", function () {
  describe("module - GraphReader", function () {
    beforeEach(function () {
      var graph = this.graph = new Graph();
      this.reader = new GraphReader("<urn:Subject>", graph);

      graph.add(new Triple("<urn:Subject>", "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>", "<urn:ObjectType>"));
      graph.add(new Triple("<urn:Subject>", "<urn:hasChildren>", "<urn:Object1>"));
      graph.add(new Triple("<urn:Subject>", "<urn:hasChildren>", "<urn:Object2>"));
      graph.add(new Triple("<urn:Subject>", "<urn:isAThing>", "'true'^^<xsd:boolean>"));
      graph.add(new Triple("<urn:Subject2>", "<urn:isAThing>", "'false'^^<xsd:boolean>"));
    });

    describe("constructor", function () {
      it("will create a graph and set subject to a BlankNode when given no params", function () {
        var test = new GraphReader();

        assert.isTrue(test.graph instanceof Graph, "Graph was initialized");
        assert.isTrue(test.subject instanceof BlankNode, "Subject was initialized");
      });

      it("will set subject to a BlankNode when given only a single Graph params", function () {
        var graph = new Graph();
        var test = new GraphReader(graph);

        assert.strictEqual(test.graph, graph, "Graph was initialized");
        assert.isTrue(test.subject instanceof BlankNode, "Subject was initialized");
      });

      it("will set subject and graph when passed in with params", function () {
        var graph = new Graph();
        var subject = new NamedNode("http://example.com/test");
        var test = new GraphReader(subject, graph);

        assert.strictEqual(test.graph, graph, "Graph was initialized");
        assert.strictEqual(test.subject, subject, "Subject was initialized");
      });
    });

    describe("function - getObject", function () {
      it("returns an array of the object of every triple with the input predicate", function () {
        var results = this.reader.getObject("<urn:Subject>", "<urn:hasChildren>");

        assert.strictEqual(results.length, 2);
        var hash = new Set();
        results.forEach(item => hash.add(item.toNT()));

        assert.isTrue(hash.has("<urn:Object1>"));
        assert.isTrue(hash.has("<urn:Object2>"));
      });
    });

    describe("function - getValue", function () {
      it("returns an array of the object of every triple with the input predicate and the Resource's Subject", function () {
        var results = this.reader.getValue("<urn:hasChildren>");

        assert.strictEqual(results.length, 2);
        var hash = new Set();
        results.forEach(item => hash.add(item.toNT()));

        assert.isTrue(hash.has("<urn:Object1>"));
        assert.isTrue(hash.has("<urn:Object2>"));
      });
    });

    describe("function - getRdfType", function () {
      it("returns an array of the  types found for the resource", function () {
        var results = this.reader.getRdfType();

        assert.strictEqual(results.length, 1);
        var hash = new Set();
        results.forEach(item => hash.add(item.toNT()));

        assert.isTrue(hash.has("<urn:ObjectType>"));
      });
    });

    describe("function - getSubject", function () {
      it("returns the subject of any triple which has the input predicate and object", function () {
        var results = this.reader.getSubject("<urn:isAThing>", '"false"^^<xsd:boolean>');

        assert.strictEqual(results.length, 1);
        var hash = new Set();
        results.forEach(item => hash.add(item.toNT()));

        assert.isTrue(hash.has("<urn:Subject2>"));
      });
    });

    describe("function - contains", function () {
      it("returns an true if the input triple is in the graph", function () {
        var results = this.reader.contains("<urn:Subject>", "<urn:hasChildren>", "<urn:Object1>");

        assert.isTrue(results);
      });

      it("returns an false if the input triple is in the graph", function () {
        var results = this.reader.contains("<urn:Subject>", "<urn:hasChildren>", "<urn:Object4>");

        assert.isFalse(results);
      });
    });

    describe("function - hasValue", function () {
      it("returns an true if the input predicate has the input value", function () {
        var results = this.reader.hasValue("<urn:hasChildren>", "<urn:Object1>");

        assert.isTrue(results);
      });

      it("returns an false if the input predicate has the input value", function () {
        var results = this.reader.hasValue("<urn:hasChildren>", "<urn:Object4>");

        assert.isFalse(results);
      });
    });

    describe("function - isOfType", function () {
      it("returns an true if the resource has the input type", function () {
        var results = this.reader.isOfType("<urn:ObjectType>");

        assert.isTrue(results);
      });

      it("returns an false if the resource has the input type", function () {
        var results = this.reader.isOfType("<urn:NotTheType>");

        assert.isFalse(results);
      });
    });
  });
});