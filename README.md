# RdfJs

An Implementation of the RDF Interfaces API, with additional helper classes and Utilities

## Installation
### Required Software

- NodeJs 5.7+
- npm 3.7+

### Deployment

    npm install

### Development

    npm install -g babel-eslint eslint
    npm install

## Testing

### Unit Tests
    >karma start
    -- or --
    >npm run test

## Recommended Reading
Here are helpful links (please add more as you find them

### RDF Interfaces
(w3 Spec) http://www.w3.org/TR/rdf-interfaces/
