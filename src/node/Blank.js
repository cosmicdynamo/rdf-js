/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import _Node from "./_Node";
import genId from '../genId';

/**
 * @class RdfJs.node.Blank
 * @see http://www.w3.org/TR/rdf-interfaces/#idl-def-BlankNode
 */
class BlankNode extends _Node {
  get interfaceName() {
    return "BlankNode";
  }

  constructor(value) {
    super(value);

    var id = this._nominalValue || genId();
    if (id.indexOf("_:") === 0) {
      id = id.substr(2);
    }
    this._nominalValue = id;
  }

  /**
   * @see http://www.w3.org/TR/rdf-interfaces/#widl-RDFNode-toString-DOMString
   * @return {string}
   */
  toString() {
    return "_:" + this._nominalValue;
  }
}

export default BlankNode;
