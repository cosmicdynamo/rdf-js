/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import typeTest from "./typeTest";

/**
 * Stores and Retrieves an object or value by a generated hash Id
 * @class blocks.HashTable
 */
class HashTable {
  constructor(genHash) {
    this._hash = new Map();
    this._lookup = new Map();

    if (genHash) {
      this.genHash = genHash;
    }
  }

  get length() {
    return this._hash.size;
  }

  /**
   * Gets the stored Object/value associated with a hash
   * @param {String} key - the access key
   * @return {*}
   */
  get(key) {
    return this._hash.get(key);
  }

  /**
   * Looks up the access key for a given object
   * @param {*} value - the object/value that may have been added
   * @return {String}
   */
  lookup(value) {
    return this._lookup.get(value);
  }

  /**
   * Adds a new Object/value to the hash and returns an access key
   * @param {*} value - The Thing being added
   * @return {String} Generated Access key
   */
  add(value) {
    var key = this._lookup.get(value);

    if (key) {
      return key;
    }

    key = this.genHash(value);

    this._hash.set(key, value);
    this._lookup.set(value, key);

    return key;
  }

  /**
   * Removes a key(s) from this HashTable
   * @param {String | String[]} keyOrList
   */
  remove(keyOrList) {
    if (typeTest.isArray(keyOrList)) {
      keyOrList.forEach(key => this.remove(key));

      return;
    }

    //Value Not Found
    var value = this._hash.get(keyOrList);
    if (!value) {
      return;
    }

    this._hash.delete(keyOrList);
    this._lookup.delete(value);
  }

  /**
   * Creates a Hash from an input object
   * @description Can be overridden by more specialized method when needed
   * @param {String | Object} key
   * @return {String}
   */
  genHash(key) {
    //check for custom toString method and use it.  Otherwise stringify
    if (typeTest.isObject(key) && key.toString() === Object.prototype.toString.call(key)) {
      var out = [];
      var keys = Object.keys(key).sort();

      keys.forEach(prop => out.push(prop + ":" + this.genHash(key[ prop ])));
      key = "{" + out.join(",") + "}";
    }
    return key.toString();
  }

  /**
   * Runs a function on every value in this table
   * @param {Function} fn - forEachCallback
   */
  forEach(fn) {
    for (var hash of this.keys()) {
      fn(this.get(hash), hash, this)
    }
  }

  /**
   * Returns all of the hash keys in this hash table
   * @returns {String[]}
   */
  keys() {
    return this._hash.keys();
  }
}

export default HashTable;
