/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
import keyWord from "../keyWord";

/**
 * Will execute an input method untill the requested range of results are received
 * @description If the min number cannot be found; the parse position will be reset and a null value returned
 * @param {*} val - the most recent value pulled from the input data
 * @param {Array<*>} list - all of the values generated so far
 * @param {blocks/parser/Data} data - Information about the parsing process
 * @param {Number} max - The maximum number of times Function will execute; or -1 if unlimited
 * @param {String} [separator] - The value that appears between each value picked up by fn
 * @param {Boolean} [optional] - Is the separator required to be between each value
 * @return {boolean} true if the parser should stop parsing
 */
export default (val, list, data, max, separator, optional) => {
  if (separator !== undefined) {
    if (keyWord(data, separator, true, true) !== null) {
      return data.isEnd();
    } else if (!optional) {
      return true;
    }
  }
  if (val === null) {
    return true;
  }
  return (max > -1 && list.length >= max) || data.isEnd();
}
