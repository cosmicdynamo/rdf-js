/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import typeTest from "../../typeTest";

/**
 * This method will execute each of the input modules in order until one returns a success result
 * @method jazzHands.parser.match#find
 * @param {blocks/parser/Data | Array<*>} data - Information about the parsing process
 * @param {Object[]} parsers -  list of module ids or instances to be tried
 * @return {Promise<*> | * | null} - Promise might be created if the module needs to be required in
 */
export default async (data, parsers) => {
  if (!typeTest.isArray(parsers)) {
    parsers = [ parsers ];
  }

  for (let idx = 0; idx < parsers.length; idx++) {
    let parser = parsers[ idx ];
    var result = await parser(data);

    if (result !== null) {
      return result;
    }
  }

  return null;
}

