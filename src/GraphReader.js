/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { BlankNode } from "./node";
import { rdf } from './prefix';
import typeTest from "./typeTest";
import Graph from "./Graph";

const toNT = (rdfNode) => {
  if (typeTest.isFunction(rdfNode.toNT)) {
    return rdfNode.toNT();
  }
  return rdfNode;
};

const init = (reader, subject, graph) => {
  reader.subject = subject;
  reader.graph = graph;
};

const initWithGraph = (reader, graph) => {
  init(reader, new BlankNode(), graph);
};

const initEmpty = reader => {
  initWithGraph(reader, new Graph())
};

class GraphReader {
  constructor(subject, graph) {
    if (arguments.length === 0) {
      initEmpty(this);
    } else if (arguments.length === 1) {
      initWithGraph(this, arguments[ 0 ]);
    } else {
      init(this, arguments[ 0 ], arguments[ 1 ]);
    }

    Object.freeze(this);
  }

  static rdfType = rdf("type");

  getValue(predicate) {
    return this.getObject(this.subject, predicate);
  }

  getObject(subject, predicate) {
    return this.graph.match(toNT(subject), toNT(predicate)).toArray().map(t => t.object);
  }

  getRdfType() {
    return this.getValue(GraphReader.rdfType);
  }

  isOfType(rdfType) {
    return this.hasValue(GraphReader.rdfType, rdfType);
  }

  hasValue(predicate, object) {
    return this.contains(this.subject, predicate, object);
  }

  contains(subject, predicate, object) {
    return this.graph.match(toNT(subject), toNT(predicate), toNT(object)).length > 0
  }

  getSubject(predicate, object) {
    return this.graph.match(null, toNT(predicate), toNT(object)).toArray().map(triple => triple.subject);
  }
}
GraphReader.toNT = toNT;

export default GraphReader;