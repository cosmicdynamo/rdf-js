﻿/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import HashTable from "./HashTable";
import typeTest from "./typeTest";
import Triple from "./Triple";
/* Implementation of <http://www.w3.org/TR/rdf-interfaces/#idl-def-Graph> */

/**
 * @class RdfJs.Graph
 */
class RdfGraph {
  constructor(params) {
    this.TripleCtr = (params && params.TripleCtr) || Triple;
    this._triples = new HashTable(t => [ t.s, t.o, t.p ].join("-"));
    this._node = new HashTable(node => typeTest.isObject(node) ? node.toNT() : node);

    this.spo = {};

    this.actions = (params && params.actions) || [];
  }

  get length() {
    return this._triples.length;
  }

  add(triple) {
    /* http://www.w3.org/TR/rdf-interfaces/#widl-Graph-add-Graph-Triple-triple */
    this.actions.forEach(function (action) {
      action(triple);
    });

    return this._add(triple);
  }

  _add(triple) {
    var spo = this.spo;

    var s = this._node.add(triple.subject.toNT());
    var p = this._node.add(triple.predicate.toNT());
    var o = this._node.add(triple.object.toNT());

    if (!(s in spo) || !(p in spo[ s ]) || !(o in spo[ s ][ p ])) {
      var ptr = this._triples.add({ s: s, p: p, o: o });

      var n = -1;
      spo[ s ] = spo[ s ] || {};
      spo[ n ] = spo[ n ] || {};

      spo[ s ][ p ] = spo[ s ][ p ] || {};
      spo[ n ][ p ] = spo[ n ][ p ] || {};
      spo[ s ][ n ] = spo[ s ][ n ] || {};
      spo[ n ][ n ] = spo[ n ][ n ] || {};

      spo[ s ][ p ][ o ] = ptr;
      spo[ n ][ p ][ o ] = spo[ n ][ p ][ o ] || [];
      spo[ n ][ p ][ o ].push(ptr);
      spo[ s ][ n ][ o ] = spo[ s ][ n ][ o ] || [];
      spo[ s ][ n ][ o ].push(ptr);
      spo[ s ][ p ][ n ] = spo[ s ][ p ][ n ] || [];
      spo[ s ][ p ][ n ].push(ptr);

      spo[ s ][ n ][ n ] = spo[ s ][ n ][ n ] || [];
      spo[ s ][ n ][ n ].push(ptr);
      spo[ n ][ n ][ o ] = spo[ n ][ n ][ o ] || [];
      spo[ n ][ n ][ o ].push(ptr);
      spo[ n ][ p ][ n ] = spo[ n ][ p ][ n ] || [];
      spo[ n ][ p ][ n ].push(ptr);

      spo[ n ][ n ][ n ] = spo[ n ][ n ][ n ] || [];
      spo[ n ][ n ][ n ].push(ptr);
    }

    return this;
  }

  remove(triple) {
    this._remove(triple);

    return this;
  }

  removeMatches(s, p, o) {
    /* http://www.w3.org/TR/rdf-interfaces/#widl-Graph-removeMatches-Graph-any-subject-any-predicate-any-object */
    this._match(s, p, o).forEach(ptr => this._removeByPtr(ptr));

    return this;
  }

  _removeByPtr(tPtr) {
    var parts = this._triples.get(tPtr);
    var s = parts.s, p = parts.p, o = parts.o;
    var spo = this.spo;
    this._triples.remove(tPtr);

    delete spo[ s ][ p ][ o ];

    function rem(idx, arr) {
      return arr.filter(val => val !== idx)
    }

    var n = -1;
    spo[ n ][ p ][ o ] = rem(tPtr, spo[ n ][ p ][ o ]);
    spo[ s ][ n ][ o ] = rem(tPtr, spo[ s ][ n ][ o ]);
    spo[ s ][ p ][ n ] = rem(tPtr, spo[ s ][ p ][ n ]);
    spo[ s ][ n ][ n ] = rem(tPtr, spo[ s ][ n ][ n ]);
    spo[ n ][ p ][ n ] = rem(tPtr, spo[ n ][ p ][ n ]);
    spo[ n ][ n ][ o ] = rem(tPtr, spo[ n ][ n ][ o ]);
    spo[ n ][ n ][ n ] = rem(tPtr, spo[ n ][ n ][ n ]);
    if (!this._inUse(s)) {
      this._node.remove(s);
    }
    if (!this._inUse(p)) {
      this._node.remove(p);
    }
    if (!this._inUse(o)) {
      this._node.remove(o);
    }
  }

  _inUse(node) {
    var spo = this.spo;
    var n = -1;
    var inList = function (s, p, o) {
      return ((spo[ s ] && spo[ s ][ p ] && spo[ s ][ p ][ o ]) || []).length > 0;
    };
    return inList(node, n, n) || inList(n, node, n) || inList(n, n, node);
  }

  _remove(triple) {
    /* http://www.w3.org/TR/rdf-interfaces/#widl-Graph-remove-Graph-Triple-triple */
    var spo = this.spo;

    var count = 0;
    var s = triple.s ? triple.s : this._lookupNode(triple.subject);
    if (s !== null && spo[ s ]) {
      var p = triple.p ? triple.p : this._lookupNode(triple.predicate);
      if (p !== null && spo[ s ][ p ]) {
        var o = triple.o ? triple.o : this._lookupNode(triple.object);
        if (o !== null && o in spo[ s ][ p ]) {
          var ptr = spo[ s ][ p ][ o ];
          this._removeByPtr(ptr);
        }
        if (spo[ s ][ p ] == {}) {
          delete spo[ s ][ p ];
        }
      }
      if (spo[ s ] == {}) {
        delete spo[ s ];
      }
    }
    return count;
  }

  toArray() {
    var out = [];
    this._triples.forEach(function (t) {
      out.push(this._expand(t));
    }.bind(this));

    return out;
  }

  _ptrToTriple(ptr) {
    if (typeTest.isArray(ptr)) {
      for (var idx = 0; idx < ptr.length; idx++) {
        ptr[ idx ] = this._ptrToTriple(ptr[ idx ]);
      }
      return ptr;
    }

    return this._expand(this._triples.get(ptr));
  }

  _expand(tPtr) {
    let subject = this._node.get(tPtr.s);
    let predicate = this._node.get(tPtr.p);
    let object = this._node.get(tPtr.o);

    return new this.TripleCtr(subject, predicate, object);
  }

  some(tFilter) {
    /* http://www.w3.org/TR/rdf-interfaces/#widl-Graph-some-boolean-TripleFilter-callback */
    var lst = this._triples.keys();
    for (var key of lst) {
      if (tFilter(this._ptrToTriple(key), this) === true) {
        return true;
      }
    }

    return false;
  }

  /**
   *
   * @param {Function} tFilter
   */
  every(tFilter) {
    /* http://www.w3.org/TR/rdf-interfaces/#widl-Graph-every-boolean-TripleFilter-callback */
    // every is false if
    return !this.some(function (t, g) {
      return (!tFilter(t, g));
    });
  }

  filter(tFilter) {
    /* http://www.w3.org/TR/rdf-interfaces/#widl-Graph-filter-Graph-TripleFilter-filter */
    var results = this._newGraph();

    var lst = this._triples.keys();
    for (var key of lst) {
      var t = this._ptrToTriple(key);
      if (tFilter(t, this) === true) {
        results.add(t);
      }
    }

    return results;
  }

  /**
   *
   * @param {Function} tCallback
   */
  forEach(tCallback) {
    /* http://www.w3.org/TR/rdf-interfaces/#widl-Graph-forEach-void-TripleCallback-callback */
    var graph = this;
    var changed = [];
    this._triples.forEach(function (t, key) {
      var value = this._expand(t);
      var before = value.toString();
      tCallback(value, graph);

      if (before !== value.toString()) {
        changed.push({
          ptr: key,
          newVal: value
        });
      }
    }.bind(this));

    changed.forEach(function (changes) {
      graph._removeByPtr(changes.ptr);
      graph._add(changes.newVal);
    });
  }

  _newGraph() {
    return new RdfGraph({
      TripleCtr: this.TripleCtr
    });
  }

  /**
   * Returns all triples which match the given input
   * @param {RdfJs.Node} [subject]
   * @param {RdfJs.Node} [predicate]
   * @param {RdfJs.Node} [object]
   * @param {Number} [limit] - Max number of results that can be returned
   * @return {RdfJs.Triple[]}
   */
  match(subject, predicate, object, limit) {
    var ptrArray = this._match(subject, predicate, object);

    if (limit) {
      ptrArray = ptrArray.slice(0, limit);
    }

    var results = this._newGraph();
    ptrArray.forEach(function (ptr) {
      results.add(this._ptrToTriple(ptr));
    }.bind(this));

    return results;
  }

  _lookupNode(node) {
    if (!node) {
      return null;
    }

    var lookup = node.toNT ? node.toNT() : node;

    return this._node.lookup(lookup);
  }

  _match(subject, predicate, object) {
    var s = this._lookupNode(subject);
    var p = this._lookupNode(predicate);
    var o = this._lookupNode(object);

    if (!subject) {
      s = -1;
    }
    if (!predicate) {
      p = -1;
    }
    if (!object) {
      o = -1;
    }

    var spo = this.spo;
    var out = spo[ s ] && spo[ s ][ p ] && spo[ s ][ p ][ o ];
    if (out === undefined) {
      out = [];
    } else if (!typeTest.isArray(out)) {
      out = [ out ];
    }
    return out;
  }

  merge(graph) {
    /* http://www.w3.org/TR/rdf-interfaces/#widl-Graph-merge-Graph-Graph-graph */
    return (new RdfGraph()).addAll(this).addAll(graph);
  }

  addAll(graph) {
    /* http://www.w3.org/TR/rdf-interfaces/#widl-Graph-addAll-Graph-Graph-graph */
    var self = this, triples;

    if (typeTest.isArray(graph)) {
      triples = graph;
    } else {
      triples = graph.toArray();
    }

    triples.forEach(function (t) {
      self.add(t);
    });

    return self;
  }

  addAction(tAction, run) {
    this.actions.push(tAction);

    if (run) {
      this.forEach(tAction);
    }
  }

  /**
   * Makes a clone of the current graph
   * @description Faster than creating a new graph
   *              because it doesn't bother converting
   *              from pointers back to triples and then
   *              re-indexing
   * @return {RdfJs.Graph}
   */
  clone() {
    return (new RdfGraph()).addAll(this);
  }

  has(triple) {
    return this.match(triple.subject.toNT(), triple.predicate.toNT(), triple.object.toNT()).length > 0;
  }
}

export default RdfGraph;
