/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import GraphReader from "./GraphReader";
import Triple from "./Triple";
import { BlankNode, Literal, NamedNode } from "./node";
import { rdf, xsd } from "./prefix";
import typeTest from "./typeTest";

const toNT = nodeOrString => {
  if (typeTest.isFunction(nodeOrString.toNT)) {
    return nodeOrString.toNT();
  }
  return nodeOrString;
};

class GraphBuilder extends GraphReader {
  addType(rdfType) {
    return this.add(rdf("type"), rdfType);
  }

  removeType(rdfType) {
    return this.remove(rdf("type"), rdfType);
  }

  add(predicate, object) {
    return this.addTriple(this.subject, predicate, object);
  }

  remove(predicate, object) {
    return this.removeTriple(this.subject, predicate, object);
  }

  set(predicate, object) {
    return this.removeAll(predicate)
      .add(predicate, object);
  }

  addTriple(subject, predicate, object) {
    this.graph.add(new Triple(subject, predicate, object));
    return this;
  }

  removeTriple(subject, predicate, object) {
    this.graph.remove(new Triple(subject, predicate, object));
    return this;
  }

  removeAll(predicate) {
    this.graph.removeMatches(toNT(this.subject), toNT(predicate), null);
    return this;
  }

  //noinspection JSMethodCanBeStatic
  createLiteral(value, langOrType) {
    return new Literal(value, langOrType);
  }

  //noinspection JSMethodCanBeStatic
  createNamed(value) {
    return new NamedNode(value);
  }

  //noinspection JSMethodCanBeStatic
  createBlank(value) {
    return new BlankNode(value);
  }
}

export default GraphBuilder;