/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* Base Class to minimize redundant code when implementing RDF PrefixMap and TermMap */
/**
 * @class RdfJs._Map
 * @mixes blocks.Container
 */
class _Map {
  constructor(init) {
    this._values = new Map();

    if (init) {
      if (init instanceof Map) {
        this._values = init;
      } else {
        Object.keys(init).forEach(key => this.set(key, init[ key ]));
      }
    }
  }

  get(alias) {
    return this._values.get(alias) || null;
  }

  set(alias, expanded) {
    this.isValid(alias);

    this._values.set(alias, expanded);
  }

  isValid(alias) {
    return alias.indexOf(" ") == -1;
  }

  addAll(add, override) {
    var map = this;
    for (var term of add.keys()) {
      if (override || map.get(term) === null) {
        map.set(term, add.get(term));
      }
    }

    return this;
  }

  /**
   * Removes an value from this Container
   * @param {String} alias
   * @returns undefined
   */
  remove(alias) {
    this._values.delete(alias);
  }

  /**
   * Returns the names of the data in this Container
   * @return {Iterator.<string>}
   */
  keys() {
    return this._values.keys();
  }
}

export default _Map;
