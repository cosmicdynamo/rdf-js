/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Cosmic Dynamo LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Cache from "./Cache";
import genId from "./genId"
import Graph from "./Graph";
import GraphReader from "./GraphReader";
import GraphBuilder from "./GraphBuilder";
import HashTable from "./HashTable";
import PrefixMap from "./PrefixMap";
import Profile from "./Profile";
import RdfType from "./RdfType";
import TermMap from "./TermMap";
import Triple from "./Triple";
import TripleStore from "./TripleStore";
import toNode from "./toNode";
import typeTest from "./typeTest";
import {BlankNode, NamedNode, Literal} from './node';
import * as prefix from "./prefix";

const rdfType = RdfType();

export {
  Cache,
  genId,
  HashTable,
  BlankNode,
  TripleStore,
  Graph,
  GraphReader,
  GraphBuilder,
  NamedNode,
  Literal,
  toNode,
  Triple,
  TermMap,
  PrefixMap,
  prefix,
  Profile,
  rdfType,
  typeTest
};

export default TripleStore;
